import { useContext, useState } from "react";
import SidebarContext from "../context/SidebarContext";
import { BsFillPersonFill } from "react-icons/bs";
import {
  HiOutlineLogout,
  HiOutlineBell,
  HiMenu,
  HiSun,
  HiMoon,
} from "react-icons/hi";
import {
  Avatar,
  Badge,
  Dropdown,
  DropdownItem,
  WindmillContext,
} from "@windmill/react-ui";
import { signOut } from "next-auth/react";
import { useFetchData } from "../lib/adapters/fetcher";
import { GrievanceType } from "../lib/types/db";
import Link from "next/link";
import { useMyProfile } from "../lib/hooks/useMyProfile";
import { Roles } from "../lib/constants/users";

function Header() {
  const { mode, toggleMode } = useContext(WindmillContext);
  const { toggleSidebar } = useContext(SidebarContext);
  const [isProfileMenuOpen, setIsProfileMenuOpen] = useState(false);
  function handleProfileClick() {
    setIsProfileMenuOpen(!isProfileMenuOpen);
  }
  const user = useMyProfile();
  const { data: grs } = useFetchData<GrievanceType[]>(
    "/api/notifications/unread"
  );
  const [isNotificationsMenuOpen, setIsNotificationsMenuOpen] = useState(
    () => false
  );
  function handleNotificationsClick() {
    setIsNotificationsMenuOpen(!isNotificationsMenuOpen);
  }

  return (
    <header className="z-40 py-4 bg-white shadow-bottom dark:bg-gray-800">
      <div className="container flex items-center justify-between h-full px-6 mx-auto text-purple-600 dark:text-purple-300">
        {/* <!-- Mobile hamburger --> */}
        <button
          className="p-1 mr-5 -ml-1 rounded-md lg:hidden focus:outline-none focus:shadow-outline-purple"
          onClick={toggleSidebar}
          aria-label="Menu"
        >
          <HiMenu className="w-6 h-6" aria-hidden="true" />
        </button>
        {/* <!-- Search input --> */}
        <div className="flex justify-center flex-1 lg:mr-32">
          <div className="relative w-full max-w-xl mr-6 focus-within:text-purple-500"></div>
        </div>
        <ul className="flex items-center flex-shrink-0 space-x-6">
          <li className="flex"><RoleBadge user={user} /></li>
          {/* <!-- Theme toggler --> */}
          <li className="flex">
            <button
              className="rounded-md focus:outline-none focus:shadow-outline-purple"
              onClick={toggleMode}
              aria-label="Toggle color mode"
            >
              {mode === "dark" ? (
                <HiSun className="w-5 h-5" aria-hidden="true" />
              ) : (
                <HiMoon className="w-5 h-5" aria-hidden="true" />
              )}
            </button>
          </li>
          {/* <!-- Notifications menu --> */}
          <li className="relative">
            <button
              className="relative align-middle rounded-md focus:outline-none focus:shadow-outline-purple"
              onClick={handleNotificationsClick}
              aria-label="Notifications"
              aria-haspopup="true"
            >
              <HiOutlineBell className="w-5 h-5" aria-hidden="true" />
              {/* <!-- Notification badge --> */}
              {grs && grs.length > 0 && (
                <span
                  aria-hidden="true"
                  className="absolute top-0 right-0 inline-block w-3 h-3 transform translate-x-1 -translate-y-1 bg-red-600 border-2 border-white rounded-full dark:border-gray-800"
                ></span>
              )}
            </button>

            <Dropdown
              align="right"
              isOpen={isNotificationsMenuOpen}
              onClose={() => false}
              className={isNotificationsMenuOpen ? "block" : "hidden"}
            >
              {grs?.map((grievance) => (
                <Link
                  href={`/grievances/view/${grievance.key}`}
                  key={grievance.key}
                >
                  <a>
                    <DropdownItem className="justify-between">
                      <span>
                        {grievance.title.slice(0, 20)}
                        {grievance.title.length > 20 ? "..." : null}
                      </span>
                      <Badge type="danger">{grievance.notifications}</Badge>
                    </DropdownItem>
                  </a>
                </Link>
              ))}
              {!grs || grs.length === 0 ? (
                <DropdownItem>No new notifications</DropdownItem>
              ) : null}
            </Dropdown>
          </li>
          {/* <!-- Profile menu --> */}
          <li className="relative">
            <button
              className="rounded-full focus:shadow-outline-purple focus:outline-none"
              onClick={handleProfileClick}
              aria-label="Account"
              aria-haspopup="true"
            >
              <Avatar
                className="align-middle"
                src={user?.image || "/images/avatar.png"}
                alt=""
                aria-hidden="true"
              />
            </button>
            <Dropdown
              align="right"
              isOpen={isProfileMenuOpen}
              onClose={() => setIsProfileMenuOpen(false)}
            >
              {/* <DropdownItem>
                <BsFillPersonFill className="w-4 h-4 mr-3" aria-hidden="true" />
                <span>Profile</span>
              </DropdownItem> */}
              <Link href={`/users/me/`}>
                <a>
                  <DropdownItem>
                    <BsFillPersonFill
                      className="w-4 h-4 mr-3"
                      aria-hidden="true"
                    />
                    <span>Profile</span>
                  </DropdownItem>
                </a>
              </Link>
              <DropdownItem onClick={() => signOut({ callbackUrl: "/login" })}>
                <HiOutlineLogout className="w-4 h-4 mr-3" aria-hidden="true" />
                <span>Log out</span>
              </DropdownItem>
            </Dropdown>
          </li>
        </ul>
      </div>
    </header>
  );
}

export default Header;


function RoleBadge ({user}: {user:any}) {
  if (user?.roles) {
    const roles = user.roles;
    return roles.includes(Roles.ADMIN) ? (
      <Badge color="red">Admin</Badge>
    ) : roles.includes(Roles.STUDENT_GREIVENCE_CELL_MEMBER) ? (
      <Badge>Student Grievance Cell Member</Badge>
    ) : roles.includes(Roles.STAFF_GREIVENCE_CELL_MEMBER) ? (
      <Badge>Staff Grievance Cell Member</Badge>
    ) : roles.includes(Roles.INTERNAL_COMPLAINTS_CELL_MEMBER) ? (
      <Badge>Internal Complaints Cell Member</Badge>
    ) : roles.includes(Roles.STAFF) ? (
      <Badge>Staff</Badge>
    ) : roles.includes(Roles.PARENT) ? (
      <Badge>Parent</Badge>
    ) : roles.includes(Roles.STUDENT) ? (
      <Badge>Student</Badge>
    ) : roles.includes(Roles.GUEST) ? (
      <Badge>Guest</Badge>
    ) : null;
  }
  return null;
}

