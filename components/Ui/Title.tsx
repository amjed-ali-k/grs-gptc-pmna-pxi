import React from 'react'

function Title({children}: {children: React.ReactNode}) {
    return (
        <h1 className="mb-2 text-xl font-semibold text-gray-700 dark:text-gray-200">
       {children}
      </h1>
    )
}

export default Title
