import React from "react";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { PublicUserType } from "../../lib/types/db";

function UserName({ id }: { id?: string }) {
  const { data } = useFetchPermenantData<PublicUserType>(`/api/users/${id}`);
  if (!data || !id) return <>Loading...</>;
  return <>{data.name}</>;
}

export default UserName;
