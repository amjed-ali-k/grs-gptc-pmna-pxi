import { HelperText, Input, Label } from "@windmill/react-ui";
import React from "react";
import { useField } from "formik";
import { InputProps } from "@windmill/react-ui/dist/Input";

interface Props extends InputProps {
  children: React.ReactNode;
  name: string;
  onChange?: (value: any) => void;
  checked?: boolean;
}
function CheckBox({ children, name, onChange, ...props }: Props) {
  const [field, meta] = useField({ ...props, name, type: "checkbox" });
  return (
    <>
      <Label className="block w-full mt-4" check>
        <Input css="" type="checkbox" {...field} onChange={(e) => {field.onChange(e); onChange && onChange(e)}} checked={field.checked}  {...props} />
        <span className="ml-2"> {children} </span>
      </Label>
      {meta.touched && meta.error ? (
        <HelperText valid={false}>{meta.error}</HelperText>
      ) : null}
    </>
  );
}

export default CheckBox;
