import { HelperText, Label } from "@windmill/react-ui";
import { useField, useFormikContext } from "formik";
import DatePicker, { ReactDatePickerProps } from "react-datepicker";
import "react-datepicker/dist/react-datepicker.css";

interface Props extends Omit<ReactDatePickerProps, "onChange" | "selected"> {
  label: React.ReactText;
  name: string;
}

export const FormDatePicker = ({ label, ...props }: Props) => {
  const { setFieldValue } = useFormikContext();
  const _props = { ...props } as any;
  const [field, meta] = useField(_props);
  return (
    <>
      <Label className="mt-4">
        <span>{label}</span>
        <DatePicker
          {...field}
          {...props}
          selected={(field.value && new Date(field.value)) || null}
          onChange={(val) => {
            setFieldValue(field.name, val);
          }}
        />
      </Label>
      {meta.touched && meta.error ? (
        <HelperText valid={false}>{meta.error}</HelperText>
      ) : null}
    </>
  );
};
export default FormDatePicker;
