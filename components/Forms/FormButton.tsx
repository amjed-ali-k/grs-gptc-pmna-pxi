import { Button } from "@windmill/react-ui";
import { ButtonProps } from "@windmill/react-ui/dist/Button";
import { useFormikContext } from "formik";
import React from "react";
import { BiLoaderAlt } from "react-icons/bi";

function FormButton({...props}: ButtonProps) {
  const { isSubmitting } = useFormikContext();
  return (
    <Button block type="submit" className="mt-4" {...props}>
      {props.children}
      {isSubmitting && (
        <span className="ml-2 animate-spin">
          <BiLoaderAlt />
        </span>
      )}
    </Button>
  );
}

export default FormButton;
