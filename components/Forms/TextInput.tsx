import { HelperText, Input, Label } from "@windmill/react-ui";
import React from "react";
import { useField } from "formik";
import { InputProps } from "@windmill/react-ui/dist/Input";

interface Props extends InputProps {
  label: React.ReactText;
  name: string;
}

function TextInput({
  label,
  ...props
}: Props) {
  const [field, meta] = useField({ ...props });
  return (
    <>
      <Label className="mt-4">
        <span>{label}</span>
        <Input css="" {...field} {...props} className="mt-1" />
      </Label>
      {meta.touched && meta.error ? (
        <HelperText valid={false}>{meta.error}</HelperText>
      ) : null}
    </>
  );
}

export default TextInput;
