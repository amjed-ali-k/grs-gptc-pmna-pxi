import { HelperText, Label, Textarea } from "@windmill/react-ui";
import React from "react";
import { useField } from "formik";
import { TextareaProps } from "@windmill/react-ui/dist/Textarea";

interface Props extends TextareaProps {
  label: React.ReactText;
  name: string;
}

function TextAreaInput({
  label,
  ...props
}: Props) {
  const [field, meta] = useField({ ...props });
  return (
    <>
      <Label className="mt-4">
        <span>{label}</span>
        <Textarea css="" {...field} {...props} className="mt-1" />
      </Label>
      {meta.touched && meta.error ? (
        <HelperText valid={false}>{meta.error}</HelperText>
      ) : null}
    </>
  );
}

export default TextAreaInput;
