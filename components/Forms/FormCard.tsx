import { Form, Formik, FormikHelpers } from "formik";
import Title from "../Ui/Title";
import FormButton from "./FormButton";
import FormWrapper from "./FormWrapper";

export function Half({ children }: { children: React.ReactNode }) {
  return <div className="px-2 break-inside">{children}</div>;
}

export function FormCard({
  children,
  title,
  description,
}: {
  children: React.ReactNode;
  title: string;
  description: string;
}) {
  return (
    <Half>
      <FormWrapper>
        <Title>{title}</Title>
        <p className="text-[12px]">{description}</p>

        <div className="w-full px-2">
          {children}
         
        </div>
      </FormWrapper>
    </Half>
  );
}

export function Masonary({ children, large }: { children: React.ReactNode; large?: boolean }) {
  return (
    <div className={`box-border mx-auto before:box-inherit after:box-inherit ${large ? "xl:masonry-2-col" : "lg:masonry-2-col 2xl:masonry-3-col"}`}>
      {children}
    </div>
  );
}

export function FormikCard({
  title,
  description,
  children,
  initialValues,
  validationSchema,
  onSubmit,
  ...props
}: {
  title: string;
  description: string;
  children: React.ReactNode;
  initialValues: any;
  validationSchema?: any;
  onSubmit: (values: any, formikHelpers: FormikHelpers<any>) => void | Promise<any>
}) {
  return (
    <FormCard title={title} description={description}>
      <Formik
        {...props}
        initialValues={initialValues}
        enableReinitialize
        onSubmit={onSubmit}
      >
        <Form>{children}  <div className="max-w-[120px]">
            <FormButton>Save</FormButton>
          </div> </Form>
      </Formik>
    </FormCard>
  );
}

