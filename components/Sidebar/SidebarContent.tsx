import React from "react";
import Link from "next/link";
import { useRouter } from "next/router";
import { signOut } from "next-auth/react";
import { Button } from "@windmill/react-ui";
import { FaSignOutAlt } from "react-icons/fa";

import routes from "../../lib/constants/sidebar";
import SidebarSubmenu from "./SidebarSubmenu";
import { Permissions, Roles } from "../../lib/constants/users";
import { useMyProfile } from "../../lib/hooks/useMyProfile";

function SidebarContent() {
  const { asPath } = useRouter();
  const profile = useMyProfile();

  return (
    <div className="py-4 text-gray-500 dark:text-gray-400">
      <Link href="/#">
        <a className="ml-6 text-lg font-bold text-gray-800 dark:text-gray-200">
          Pixie - GRS
        </a>
      </Link>
      <div className="pt-2" />
      {routes.map((group) => {
        if (!profile) return null;
        if (!visible(group, profile)) return null;

        return (
          <div key={group.title}>
            <h4 className="px-2 pb-1 mx-4 mt-8 mb-2 text-[10px] font-semibold text-opacity-50 uppercase border-b-2 dark:border-gray-700">
              {group.title}
            </h4>
            <ul>
              {group.menu.map((route) => {
                const Icon = route.icon;
                if (
                  route.permissions && profile.permissions && 
                  !check(route.permissions, profile.permissions)
                )
                  return null;
                if (route.roles && profile.roles &&  !check(route.roles, profile.roles))
                  return null;
                return route.routes ? (
                  <SidebarSubmenu route={route} key={route.name} />
                ) : (
                  <li className="relative px-6 py-1" key={route.name}>
                    <Link href={route.path || "#"}>
                      <a
                        className={`inline-flex items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200 ${
                          asPath == route.path
                            ? "dark:text-gray-100 text-gray-800"
                            : ""
                        }`}
                      >
                        {asPath == route.path && (
                          <span
                            className="absolute inset-y-0 left-0 w-1 bg-purple-600 rounded-tr-lg rounded-br-lg"
                            aria-hidden="true"
                          ></span>
                        )}

                        <Icon className="w-5 h-5" aria-hidden="true" />
                        <span className="ml-4">{route.name}</span>
                      </a>
                    </Link>
                  </li>
                );
              })}
            </ul>
          </div>
        );
      })}
      <ul>
        <li className="relative px-6 py-3">
          <a
            onClick={() => signOut({ callbackUrl: "/login" })}
            className={`inline-flex cursor-pointer   items-center w-full text-sm font-semibold transition-colors duration-150 hover:text-gray-800 dark:hover:text-gray-200    : ""
                  }`}
          >
            <FaSignOutAlt className="w-5 h-5" aria-hidden="true" />
            <span className="ml-4">Logout</span>
          </a>
        </li>
      </ul>
      <div className="px-6 my-6">
        <Link href="/grievances/new">
          <a>
            <Button>
              New Grievance
              <span className="ml-2" aria-hidden="true">
                +
              </span>
            </Button>
          </a>
        </Link>
      </div>
    </div>
  );
}

export default SidebarContent;

const check = (arr1: any[], arr2: any[]) =>
  arr1.some((r) => arr2.indexOf(r) >= 0);

const visible = (group: any, profile: any) => {
  const allowedRoles = new Set<Roles>();
  group.menu.forEach((menu: any) => {
    menu?.roles?.forEach((element: any) => {
      allowedRoles.add(element);
    });
  });
  const allowedPermissions = new Set<Permissions>();
  group.menu.forEach((menu: any) => {
    menu?.permissions?.forEach((element: any) => {
      allowedPermissions.add(element);
    });
  });
  if(!profile.permissions || !profile.roles) return null
  if (allowedPermissions.size>0 && profile.permissions && !check(Array.from(allowedPermissions), profile.permissions)) return null;
  if (allowedRoles.size>0 && profile.roles && !check(Array.from(allowedRoles), profile.roles)) return null;
  return true;
};
