import Head from 'next/head'
import React from 'react'

interface IPageTitle{
  children: React.ReactText | null | undefined
}

function PageTitle({ children }: IPageTitle) {
  if(!children) return <div className='h-10 my-6 bg-gray-300 rounded-lg dark:bg-gray-600 animate-pulse'></div>
  return (
    <h1 className="my-6 text-2xl font-semibold text-gray-700 dark:text-gray-200">
      <Head>
        <title>{children} - Pixie GRS</title>
      </Head>
      {children}</h1>
  )
}

export default PageTitle
