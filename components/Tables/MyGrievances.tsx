import React from "react";
import { useFetchData } from "../../lib/adapters/fetcher";
import { GrievanceType } from "../../lib/types/db";
import GrievanceTable from "./GrievanceTable";

function MyGrievances() {
  const { data } = useFetchData<GrievanceType[] | null>("/api/grievances/mine");
  if (!data)
    return (
      <div className="w-full h-20 my-6 bg-gray-300 rounded-lg dark:bg-gray-600 animate-pulse"></div>
    );
  return <GrievanceTable grievances={data} />;
}

export default MyGrievances;
