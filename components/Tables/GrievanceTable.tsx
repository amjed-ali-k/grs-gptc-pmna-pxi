import React from "react";

import {
  TableBody,
  TableContainer,
  Table,
  TableHeader,
  TableCell,
  TableRow,
  TableFooter,
  Badge,
  Pagination,
  Input,
  Select,
} from "@windmill/react-ui";
import {
  useTable,
  useSortBy,
  usePagination,
  useFilters,
  useGlobalFilter,
  useAsyncDebounce,
} from "react-table";

import {
  TiArrowSortedDown,
  TiArrowSortedUp,
  TiArrowUnsorted,
} from "react-icons/ti";
import { GrievanceType } from "../../lib/types/db";
import { useRouter } from "next/router";
import { FaMehBlank, FaSearch } from "react-icons/fa";
import _d from "dayjs";

export default GrievanceTable;

function GrievanceTable({
  grievances,
  showNew = true,
}: {
  grievances: GrievanceType[] | undefined;
  showNew?: boolean;
}) {
  const router = useRouter();

  const cols = React.useMemo(
    () => [
      {
        Header: "Subject",
        accessor: "subject", // accessor is the "key" in the data
      },
      {
        Header: "Priority",
        accessor: "priority",
      },
      {
        Header: "Responses",
        accessor: "responses",
      },
      {
        Header: "Status",
        accessor: "status",
      },
      {
        Header: "Date",
        accessor: "date",
      },
    ],
    []
  ) as any;
  const data = React.useMemo(
    () =>
      grievances?.map((e) => ({
        subject: e.title,
        priority: e.priority,
        responses: e.totalResponses,
        status: e.status,
        date: new Date(e.createdAt).toISOString(),
        key: e.key,
        notifications: e.notifications,
      })),
    [grievances]
  );

  const tableInstance = useTable(
    { columns: cols, data: data ? data : [] },
    useFilters, // useFilters!
    useGlobalFilter, // useGlobalFilter!
    useSortBy,
    usePagination
  );
  const {
    getTableProps,
    getTableBodyProps,
    headerGroups,
    prepareRow,
    page,
    gotoPage,
    rows,
    setFilter,
    setGlobalFilter,
    preFilteredRows,
    state: { pageSize, globalFilter },
  } = tableInstance;
  const gfProps = { setGlobalFilter, globalFilter };
  return (
    <TableContainer>
      <div className="flex justify-between">
        <SearchBar {...gfProps} />
        <StatusColumnFilter
          setFilter={setFilter}
          preFilteredRows={preFilteredRows}
        />
      </div>
      <Table {...getTableProps()}>
        <TableHeader>
          {
            // Loop over the header rows
            headerGroups.map((headerGroup) => (
              // Apply the header row props
              <tr
                {...headerGroup.getHeaderGroupProps()}
                key={headerGroup.getHeaderGroupProps().key}
              >
                {
                  // Loop over the headers in each row
                  headerGroup.headers.map((column: any) => (
                    // Apply the header cell props
                    <TableCell
                      {...column.getHeaderProps(column.getSortByToggleProps())}
                      key={
                        column.getHeaderProps(column.getSortByToggleProps()).key
                      }
                    >
                      <div className="flex items-center">
                        {
                          // Render the header
                          column.render("Header")
                        }
                        <span>
                          {column.isSorted ? (
                            column.isSortedDesc ? (
                              <TiArrowSortedDown />
                            ) : (
                              <TiArrowSortedUp />
                            )
                          ) : (
                            <TiArrowUnsorted />
                          )}
                        </span>
                      </div>
                    </TableCell>
                  ))
                }
              </tr>
            ))
          }
        </TableHeader>
        <TableBody {...getTableBodyProps()}>
          {
            // Loop over the table rows
            page.map((row) => {
              // Prepare the row for display
              prepareRow(row);
              return (
                // Apply the row props
                <TableRow
                  className="cursor-pointer"
                  {...row.getRowProps()}
                  key={row.getRowProps().key}
                  onClick={() =>
                    router.push(`/grievances/view/${row.original.key}`)
                  }
                >
                  <TableCell {...row.cells[0].getCellProps()}>
                    <div className="flex items-center text-sm">
                      <div>
                        <p className="font-semibold">
                          {row.cells[0].render("Cell")}{" "}
                          {showNew && row.original.notifications > 0 ? (
                            <Badge type="danger">
                              {row.original.notifications} New responses
                            </Badge>
                          ) : null}
                        </p>
                        {/* <p className="text-xs text-gray-600 dark:text-gray-400">
                          Lorem ipsum dolor sit amet consectetur adipisicing
                          elit. Nihil...
                        </p> */}
                      </div>
                    </div>
                  </TableCell>
                  <TableCell {...row.cells[1].getCellProps()}>
                    <span className="text-sm">
                      {row.cells[1].render("Cell")}
                    </span>
                  </TableCell>
                  <TableCell {...row.cells[2].getCellProps()}>
                    <span className="text-sm">
                      {row.cells[2].render("Cell")}{" "}
                    </span>
                  </TableCell>
                  <TableCell {...row.cells[3].getCellProps()}>
                    <Badge type="primary"> {row.cells[3].render("Cell")}</Badge>
                  </TableCell>
                  <TableCell {...row.cells[4].getCellProps()}>
                    <span className="text-sm">
                      {_d(new Date(row.original.date)).format("D MMM YYYY hh:mm A")}
                    </span>
                  </TableCell>
                </TableRow>
              );
            })
          }
        </TableBody>
      </Table>
      {rows.length === 0 && (
        <div className="flex items-center justify-center w-full my-3 text-lg text-center">
          <FaMehBlank className="mr-2" size={30} /> No Grievances found{" "}
        </div>
      )}
      <TableFooter>
        <Pagination
          totalResults={rows.length}
          resultsPerPage={pageSize}
          label="Table navigation"
          onChange={(e) => {
            gotoPage(e);
          }}
        />
      </TableFooter>
    </TableContainer>
  );
}

function SearchBar({ setGlobalFilter, globalFilter }: any) {
  const [value, setValue] = React.useState(globalFilter);
  const onChange = useAsyncDebounce((_v) => {
    setGlobalFilter(_v || undefined);
  }, 200);

  return (
    <div className="flex justify-start flex-1 mx-2 my-3">
      <div className="relative w-full max-w-xl mr-6 focus-within:text-purple-500">
        <div className="absolute inset-y-0 flex items-center pl-2">
          <FaSearch className="w-4 h-4" aria-hidden="true" />
        </div>
        <Input
          css=""
          value={value || ""}
          onChange={(e) => {
            setValue(e.target.value);
            onChange(e.target.value);
          }}
          placeholder="Search for grievances"
          aria-label="Search"
          className="pl-8 text-gray-700"
        />
      </div>
    </div>
  );
}

function StatusColumnFilter({ setFilter, preFilteredRows }: any) {
  // Calculate the options for filtering
  // using the preFilteredRows
  const options = React.useMemo(() => {
    const _options = new Set<string>();
    preFilteredRows.forEach((row: any) => {
      _options.add(row.values["status"]);
    });
    return Array.from(_options);
  }, [preFilteredRows]);

  // Render a multi-select box
  return (
    <span className="inline-flex items-center mx-4">
      {" "}
      <div className="w-full text-sm">Filter Status :</div>
      <Select
        css=""
        className="flex-shrink my-3"
        onChange={(e) => {
          setFilter("status", e.target.value || undefined);
        }}
      >
        <option value="">All</option>
        {options.map((option, i) => (
          <option key={i} value={option}>
            {option}
          </option>
        ))}
      </Select>
    </span>
  );
}
