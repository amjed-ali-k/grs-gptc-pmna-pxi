import React from "react";
import * as Yup from "yup";
import toast, { Toaster } from "react-hot-toast";
import { Form, Formik } from "formik";

import Title from "../Ui/Title";
import httpClient from "../../lib/helpers/httpClient";
import FormButton from "../Forms/FormButton";
import SelectInput from "../Forms/SelectInput";
import TextInput from "../Forms/TextInput";
import { parentProfileSchema } from "../../lib/schemas/profile";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { AuthSettings, SettingsType } from "../../lib/constants/settings";

interface InitialValues {
  department?: string;
  student_name?: string;
  semester?: string;
  email?: string;
  register_no?: string;
  institution?: string;
}

interface Props {
  initialValues?: InitialValues;
  onSuccess?: () => Promise<void>;
  email?: string;
}

const _initialValues = {
  student_name: "",
  department: "",
  semester: "",
  register_no: "",
  institution: "",
  email: "",
  type: "parent",
};

function ParentProfileEditForm({
  initialValues = _initialValues,
  onSuccess,
  email,
}: Props) {
  if (email) initialValues.email = email;
  const { data: settings } = useFetchPermenantData<AuthSettings>(
    `/api/settings/auth`
  );
  return (
    <Formik
      initialValues={{
        ...initialValues,
      }}
      validationSchema={Yup.object(parentProfileSchema)}
      enableReinitialize
      onSubmit={async (values, { setSubmitting }) => {
        try {
          const res = await httpClient.post("/api/users/profile/parent/", values);
          console.log(res)
          onSuccess && await onSuccess();
          toast.success("Profile successfully updated!");
        } catch (error) {
          toast.error("An error occured!");
        }
        setSubmitting(false);
      }}
    >
      <Form>
        <Toaster />
        <Title>Profile Details</Title>
        <div className="flex flex-wrap mb-8">
          <MediumBlock>
            <TextInput name="student_name" type="text" label="Student Name" />
          </MediumBlock>
          <MediumBlock>
            <SelectInput name="department" label="Department">
              {settings?.departments.map((department) => (
                <option key={department} value={department}>
                  {department.replace(/_/g, " ")}
                </option>
              ))}
            </SelectInput>
          </MediumBlock>
          <MediumBlock>
            <SelectInput name="semester" label="Semester">
              {settings?.semesters.map((semester) => (
                <option key={semester} value={semester}>
                  {semester.replace(/_/g, " ")}
                </option>
              ))}
            </SelectInput>
          </MediumBlock>
          <MediumBlock>
            <TextInput
              name="register_no"
              type="text"
              label="Student Register No."
            />
          </MediumBlock>
          <MediumBlock>
            <TextInput name="institution" type="text" label="Institution" />
          </MediumBlock>
        </div>
        <div className="flex max-w-sm mb-4 ml-2">
          <FormButton>Update Profile</FormButton>
        </div>
      </Form>
    </Formik>
  );
}

export default ParentProfileEditForm;

function MediumBlock({
  children,
  block = false,
}: {
  children: React.ReactNode;
  block?: boolean;
}) {
  const comp = (
    <div className="w-full px-2 my-2 lg:w-1/2 2xl:w-1/3">{children}</div>
  );

  return block ? <div className="w-full">{comp}</div> : comp;
}
