import React from "react";
import _d from "dayjs";
import { RiListCheck2 } from "react-icons/ri";

import httpClient from "../../lib/helpers/httpClient";
import { GResponseType } from "../../lib/types/db";
import UserCard from "../Ui/UserCard";
import { useMyProfile } from "../../lib/hooks/useMyProfile";
import toast from "react-hot-toast";
import { Permissions } from "../../lib/constants/users";
import { GrTypeType } from "../../lib/constants/grievance";

function ResponsesList({
  grievanceId,
  grievanceType,
  reload,
}: {
  grievanceId?: string;
  grievanceType?: string;
  reload: boolean;
}) {
  const [rsps, setRsps] = React.useState<GResponseType[] | null>(() => null);
  const [refresh, setRefresh] = React.useState<boolean>(() => false);
  // TODO: Convert below fetch to useSWR
  React.useEffect(() => {
    if (grievanceId) {
      httpClient
        .get<GResponseType[]>(`/api/grievances/responses/${grievanceId}`)
        .then((res) => {
          let _arr = res.data;
          _arr.reverse();
          setRsps(_arr);
        });
    }
  }, [grievanceId, reload, refresh]);

  async function deleteResponse(r: string) {
    await httpClient.post(`/api/responses/delete/`, {
      responseId: r,
      grievanceId,
    });
    setRefresh(!refresh);
    toast.success("Response deleted successfully");
  }
  const user = useMyProfile();

  const allowDelete =
    (grievanceType === GrTypeType.STAFF &&
      user?.permissions.includes(
        Permissions.DELETE_STAFF_GRIEVANCE_RESPONSE
      )) ||
    (grievanceType === GrTypeType.STUDENT &&
      user?.permissions.includes(
        Permissions.DELETE_STUDENT_GRIEVANCE_RESPONSE
      )) ||
    (grievanceType === GrTypeType.INTERNAL &&
      user?.permissions.includes(
        Permissions.DELETE_INTERNAL_COMPLAINTS_RESPONSE
      ));

  if (rsps && rsps.length == 0) {
    return (
      <div className="p-3 border-t-2 dark:border-gray-800 ">
        {" "}
        <p className="flex items-center">
          <span className="mr-3">
            <RiListCheck2 size={60} />
          </span>
          No responses yet
        </p>
      </div>
    );
  }

  return (
    <div>
      {rsps ? (
        rsps.map((r) => (
          <Response
            key={r.key}
            response={r}
            grievanceId={grievanceId}
            deleteResponse={deleteResponse}
            allowDelete={allowDelete}
          />
        ))
      ) : (
        <>
          <Rloading />
          <Rloading />
          <Rloading />
        </>
      )}
    </div>
  );
}

export default ResponsesList;

function Response({
  response,
  deleteResponse,
  allowDelete = false,
}: {
  response: GResponseType;
  grievanceId?: string;
  deleteResponse: (r: string) => void;
  allowDelete?: boolean;
}) {
  const user = useMyProfile();
  const canDelete = user?.id == response.userId || allowDelete;

  const [deleteConfirm, setDeleteConfirm] = React.useState(false);
  const [deleting, setDeleting] = React.useState(false);

  function handleDelete() {
    if (deleteConfirm) {
      setDeleting(true);
      deleteResponse(response.key);
    }
  }

  return (
    <div className="relative p-3 border-t-2 group dark:border-gray-800" onMouseLeave={()=>setDeleteConfirm(false)}>
      {canDelete && (
        <div className="absolute right-0 text-xs transition-opacity duration-500 ease-in-out opacity-0 cursor-pointer group-hover:opacity-100 top-2">
          <span
            className="text-red-600 hover:text-red-400"
            onClick={() => setDeleteConfirm(true)}
          >
            {deleting
              ? "Deleting..."
              : deleteConfirm
              ? "Are you sure? "
              : "Delete"}
          </span>
          {deleteConfirm && !deleting && (
            <>
              <span
                onClick={handleDelete}
                className="ml-1 text-red-600 hover:text-red-400"
              >
                Yes
              </span>{" "}
              <span
                onClick={() => setDeleteConfirm(false)}
                className="ml-1 text-green-600 hover:text-green-400"
              >
                No
              </span>
            </>
          )}
        </div>
      )}
      <p>{response.content}</p>
      <div className="flex items-end justify-between">
        <UserCard id={response.userId} />
        <div className="">
          <p className="text-xs">
            Posted: {_d(response.createdAt).format("DD/MM/YYYY hh:mm A")}
          </p>
          <p className="text-xs capitalize">Status: {response.status}</p>
        </div>
      </div>
    </div>
  );
}
Response.Loading = Rloading;
function Rloading() {
  return (
    <div className="p-3 border-t-2 dark:border-gray-800 ">
      {" "}
      <div className="h-24 bg-gray-700 rounded-sm animate-pulse"></div>
    </div>
  );
}
