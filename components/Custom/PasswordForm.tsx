import React from "react";
import * as Yup from "yup";
import toast, { Toaster } from "react-hot-toast";
import { Form, Formik } from "formik";

import Title from "../Ui/Title";
import httpClient from "../../lib/helpers/httpClient";
import FormButton from "../Forms/FormButton";
import TextInput from "../Forms/TextInput";
import { useMyProfile } from "../../lib/hooks/useMyProfile";

interface InitialValues {
  current_password: string;
  password: string;
  repeat_password: string;
  email?: string;
}

interface Props {
  initialValues?: InitialValues;
  onSuccess?: () => void;
  isNew?: boolean;
  email?: string;
}

const _initialValues = {
  current_password: "",
  password: "",
  repeat_password: "",
};

function PasswordForm({
  initialValues = _initialValues,
  onSuccess,
  email,
  isNew = false,
}: Props) {
  const schema = Yup.object({
    email: Yup.string().email("Invalid email address").required("Required"),
    current_password: Yup.string().required("Required"),
    password: Yup.string().required("Required"),
    repeat_password: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });
  const newSchema = Yup.object({
    password: Yup.string().required("Required"),
    repeat_password: Yup.string().oneOf(
      [Yup.ref("password"), null],
      "Passwords must match"
    ),
  });
  
  if (email) initialValues.email = email;

  return (
    <Formik
      initialValues={{
        ...initialValues,
      }}
      validationSchema={isNew ? newSchema : schema}
      enableReinitialize
      onSubmit={async (values, { setSubmitting }) => {
       httpClient.post(`/api/users/change-password/`, values)
          .then((res) => {
            toast.success("Successfully updated password!");
            onSuccess?.();
          })
          .catch((err) => {
            toast.error(err.data.error);
          });
        setSubmitting(false);
      }}
    >
      <Form>
        <Toaster />
        <Title>Password update</Title>
        <div className="flex flex-wrap mb-8">
          {!isNew && (
            <MediumBlock>
              <TextInput
                name="current_password"
                type="password"
                label="Current Password"
              />
            </MediumBlock>
          )}
          <MediumBlock>
            <TextInput name="password" type="password" label="New Password" />
          </MediumBlock>
          <MediumBlock>
            <TextInput
              name="repeat_password"
              type="password"
              label="Repeat Password"
            />
          </MediumBlock>
        <div className="max-w-sm">
          <FormButton>Update Password</FormButton>
        </div>
        </div>
      </Form>
    </Formik>
  );
}

export default PasswordForm;

function MediumBlock({
  children,
  block = false,
}: {
  children: React.ReactNode;
  block?: boolean;
}) {
  const comp = (
    <div className="w-full px-2 my-2 lg:w-1/2 2xl:w-1/3">{children}</div>
  );

  return block ? <div className="w-full">{comp}</div> : comp;
}
