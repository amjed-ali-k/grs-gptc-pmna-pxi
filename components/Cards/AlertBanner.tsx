import React from "react";

function AlertBanner({ children }: { children: React.ReactNode }) {
  return (
    <div className="flex items-center justify-between p-4 mb-8 text-sm text-purple-100 bg-purple-600 rounded-lg shadow-md focus:outline-none focus:shadow-outline-purple">
      <div className="flex items-center">{children}</div>
    </div>
  );
}

export default AlertBanner;
