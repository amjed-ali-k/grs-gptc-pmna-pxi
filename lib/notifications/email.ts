import ejs from "ejs";
import sendGridMail, { MailDataRequired } from "@sendgrid/mail";
import { GResponseType, GrievanceType } from "../types/db";
import { getUser } from "../db/auth";

sendGridMail.setApiKey(
  process.env.SENDGRID_API_KEY ? process.env.SENDGRID_API_KEY : ""
);

async function sendEmail(data: MailDataRequired) {
  try {
    const res = await sendGridMail.send(data);
    console.log(res);
  } catch (err) {
    console.log(err);
    // Add Log - Email Error
  }
}

export async function sendRegistrationEmail(user: {
  name: string;
  email: string;
}) {
  ejs.renderFile(
    `lib/notifications/emails/registration.ejs`,
    {
      host: "localhost:3000",
      name: user.name,
      email: user.email,
      title: "Registered",
    },
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        sendEmail({
          to: user.email,
          from: "test@fdg-capital.com",
          subject: "You have been registered successfully",
          html: data,
        });
      }
    }
  );
}

export async function sendNewGrievanceEmail(grievance: GrievanceType) {
  ejs.renderFile(
    "lib/notifications/emails/newGrievance.ejs",
    {
      host: "localhost:3000",
      grievance: {
        key: grievance.key,
        subject: grievance.title,
        status: grievance.status,
        date: grievance.createdAt,
      },
    },
    (err, data) => {
      if (err) {
        console.log(err);
      } else {
        sendEmail({
          to: grievance.userEmail,
          from: "test@fdg-capital.com",
          subject: "New Grievance successfully created!",
          html: data,
        });
      }
    }
  );
}

export async function sendNewResponseEmail(response: GResponseType, grievance: GrievanceType) {
    ejs.renderFile(
      "lib/notifications/emails/newGrievance.ejs",
      {
        host: "localhost:3000",
        response: {
          key: (await getUser(response.userId))?.name,
          content: response.content,
          status: response.status,
          date: response.createdAt,
        },
      },
      (err, data) => {
        if (err) {
          console.log(err);
        } else {
          sendEmail({
            to: grievance.userEmail,
            from: "test@fdg-capital.com",
            subject: "New Grievance successfully created!",
            html: data,
          });
        }
      }
    );
  }

