import type { AdapterSession, VerificationToken, AdapterUser } from "next-auth/adapters";
import type { Account } from "next-auth";
import * as detaDb from "../db/auth";

/** @return { import("next-auth/adapters").Adapter } */
export default function DetaAdapter(client: null, options = {}) {
  return {
    async createUser(user: AdapterUser) {
      return detaDb.createUser(user);
    },
    async getUser(id: string) {
      return detaDb.getUser(id);
    },
    async getUserByEmail(email: string) {
      return detaDb.getUserByEmail(email);
    },
    async getUserByAccount({ providerAccountId }: { providerAccountId: string }) {
      return detaDb.getUserByAccount(providerAccountId);
    },
    async updateUser(user: AdapterUser) {
      return detaDb.updateUser(user);
    },
    async deleteUser(userId: string) {
      return detaDb.deleteUser(undefined, userId);
    },
    async linkAccount(account: Account) {
      return detaDb.linkAccount(account);
    },
    async unlinkAccount({ id }: { id: string }) {
      return detaDb.unlinkAccount(id);
    },
    async createSession(session: AdapterSession) {
      return detaDb.createSession(session);
    },
    async getSessionAndUser(sessionToken: string) {
      return detaDb.getSessionAndUser(sessionToken);
    },
    async updateSession(session: AdapterSession) {
      return detaDb.updateSession(session);
    },
    async deleteSession(sessionToken: string) {
      return detaDb.deleteSession(sessionToken);
    },
    async createVerificationToken(token: VerificationToken) {
      return detaDb.createVT(token);
    },
    async useVerificationToken({ identifier }: VerificationToken) {
      return detaDb.useVT(identifier);
    },
  };
}
