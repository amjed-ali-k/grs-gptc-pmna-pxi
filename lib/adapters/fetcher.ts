import axios from 'redaxios'
import useSWR, { Key } from 'swr'
import useSWRImmutable from 'swr/immutable'

export const fetcher = (url:string) => axios.get(url).then(res => res.data)

export const useFetchData = <T>(url: string) => {
  return useSWR<T>(url, fetcher)
}

export const useFetchPermenantData = <T>(url:Key) => {
  return useSWRImmutable<T>(url, fetcher)
}