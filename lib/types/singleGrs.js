// playground requires you to assign document definition to a variable called dd

var dd = {
  
  content: [
    {
      text: "Delay of the salary",
      style: "header",
    },
    {
      columns: [
        {
          text: [
            { text: "Amjed Ali", fontSize: 13, bold: true },
            "\n",
            "Tradesman",
            "\n",
            "Electronics Department",
          ],
          width: "30%",
          style: "fontXs",
        },
        {
          text: [
            {
              text: "Created on 12-Jan-2022 12:43 PM",
            },
            "\n",
            {
              text: "Updated on 02-Jan-2022 12:52 PM",
            },
            "\n",
            "ID: 982394279224",
            "\n",
            "",
          ],
          width: "30%",
          style: "fontXs",
        },
        {
          text: [
            {
              text: "Status:",
              fontSize: 14,
            },
            {
              text: "Closed",
              style: "bold",
              fontSize: 14,
            },
            "\n",
            "High Priority",
            "\n",
            {
              text: "Academic Related",
              fontSize: 11,
              style: "boldItalic",
            },
          ],
          style: "headerRight",
        },
      ],
    },
    {
      table: {
        widths: ["*"],
        margin: [0, 15, 0, 15],
        style: "tableExample",
        body: [
          [
            "I am writing this letter to inform you that I have not received my salary for the month of December 2021. The matter is a serious concern for me as delay in salary causes me financial problems. I have contacted the accounting dept but they have replied with no policy update. Please investigate and resolve any issue that is delaying the salary payment"
          ]
        ],
      },
    },
    {
      text: "Responses",
      style: "subheader",
    },
    {
      table: {
        widths: ["*",  "auto"],
        style: "tableExample",
        body: [
         
          [
            "We have throughly went through the issue. It seems clerical delay. We will update you once it is processed.",
           
            {
              text: "Amjed Ali",
              alignment: "right",
            },
          ],
          [
            "Grievance is assigned to a staff",
           
            {
              text: "Amjed Ali",
              alignment: "right",
            },
          ],
        ],
      },
    },
  
  ],
};
