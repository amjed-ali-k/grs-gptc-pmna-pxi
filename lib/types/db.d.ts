import { AdapterUser } from "next-auth/adapters";
import { Statuses, GrTypeType } from "../constants/grievance";

export interface UserTypeInDB extends UserType {
  key: string;
  updatedAt?: string;
}

interface BaseUserType {
  updatedAt?: number;
  email: string;
  image: string;
  name: string;
  id: string;
  key: string;
  createdAt?: number;
}

interface CommonType {
  email: string;
  type: string;
  updatedAt: string;
  key: string;
  password?: string;
}

export interface StaffUserType extends CommonType {
  designation?: string;
  department?: string;
  pen_no?: string;
  type: "staff";
}

export interface ParentUserType extends CommonType {
  department?: string;
  student_name?: string;
  semester?: string;
  register_no?: string;
  type: "parent";
}

export interface StudentUserType extends CommonType {
  department?: string;
  semester?: string;
  year_of_passout?: string;
  register_no?: string;
  type: "student";
}

export type UserType = BaseUserType &
  (StaffUserType | StudentUserType | ParentUserType);

export type ProfileType = StaffUserType | StudentUserType | ParentUserType;

export type PublicUserType = AdapterUser & {
  designation: string;
  department: string;
};

export type RoleType = {
  key: string;
  roles: string[];
};

export interface AuthUserType {
  emailVerified: Date | null;
  id: string;
  image: string;
  name: string;
  email: string;
}

export interface NewGrievanceApi {
  title: string;
  content: string;
  priority: string;
  category: string;
  type: string;
}

export interface GrievanceInput extends NewGrievanceApi {
  userId: string;
  userEmail: string;
  status?: string;
  type: GrTypeType;
  ip: string | null;
}

export interface GrievanceType extends GrievanceInput {
  key: string;
  pinned: boolean;
  createdAt: number;
  updatedAt: number;
  status: Statuses;
  totalResponses: number;
  notifications: number;
  unReadResponseIds: string[];
}

export interface NewResponseApi {
  content: string;
  grievanceId: string;
  status: string;
}

export interface ResponseInput extends NewResponseApi {
  userId: string;
  userEmail: string;
  ip: string | null;
  prevStatus: Statuses;
  status: Statuses;
}

export interface GResponseType extends ResponseInput {
  key: string;
  createdAt: number;
}

export interface PermissionsType {
  key?: string;
  permissions: string[];
}

interface hasPassword {
  hasPassword: boolean;
}

export type UserProfileFullType = Omit<UserType, "password"> &
  PermissionsType &
  RoleType &
  hasPassword;

export type UserWithProfile = UserType & ProfileType;

export interface GrieavancesWithResponses extends GrievanceType {
  responses: GResponseType[];
}
