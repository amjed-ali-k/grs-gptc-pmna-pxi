import { Deta } from "deta";

// Initialize with a Project Key
const deta = Deta(process.env.DETA_PROJECT_KEY);

export const usersDB = deta.Base("auth_users");
export const accountsDB = deta.Base("auth_user_accounts");
export const sessionDB = deta.Base("auth_user_sessions");
export const tokenDB = deta.Base("auth_user_token");

export const profileDB = deta.Base("user_profile");
export const rolesDB = deta.Base("user_roles")
export const permissionsDB = deta.Base("settings_role_permissions")

export const deletedDB = deta.Base("_deleted_");


// Grievance Related DBs

export const grievanceDB = deta.Base("grievances");
export const responsesDB = deta.Base("responses");
export const logsDB = deta.Base("logs");
export const reportsDB = deta.Base("reports");

export const settingsDB = deta.Base("system_settings");