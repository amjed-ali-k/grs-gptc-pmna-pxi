import { GrTypeType } from "../constants/grievance";
import {
  GrievanceInput,
  GrievanceType,
  ResponseInput,
  GResponseType,
} from "../types/db";
import { deletedDB, grievanceDB, responsesDB } from "./db";
import { LogType, newLog } from "./logs";

export async function createNewGrievance(grievance: GrievanceInput) {
  return grievanceDB.put({
    ...grievance,
    status: "open",
    totalResponses: 0,
    pinned: false,
    updatedAt: Date.now(),
    createdAt: Date.now(),
    notifications: 0,
    unReadResponseIds: [],
    key: (9000000000000 - Date.now()).toString(),
  }) as unknown as GrievanceType | null;
}

export async function updateGrievance(grievance: {}, key: string) {
  const _gr = await getGrievance(key);
  return grievanceDB.put({..._gr, ...grievance}) as unknown as GrievanceType | null;
}

export async function getGrievance(id: string) {
  return grievanceDB.get(id) as unknown as GrievanceType | null;
}

export async function getAllGrievanceForEmail(userEmail: string) {
  const grs = await grievanceDB.fetch({ userEmail });
  if (grs.count < 1) return [];
  return grs.items as unknown as GrievanceType[];
}

export async function getAllGrievances(query?: any){
    const grs = await grievanceDB.fetch(query);
    if (grs.count < 1) return [];
    return grs.items as unknown as GrievanceType[];
}

export async function getAllGrievancesBetweenDates(fromDate: Date, toDate: Date, type: GrTypeType) {
  console.log({fromDate, toDate, type})
  const grs = await grievanceDB.fetch({"createdAt?r": [fromDate.getTime(), toDate.getTime()], type})
  console.log(grs)
  return grs.items as unknown as GrievanceType[]  
}

export async function deleteGrievance(grievance_key: string) {
  const gr = await getGrievance(grievance_key);
  if (!gr) return null;
  await deletedDB.put({ ...gr }, `deleted_grievance_${gr.key}`);
  await grievanceDB.delete(grievance_key);
  await newLog(LogType.DELETE_GRIEVANCE, {id: gr.key}, gr.userId, gr.type);
  const grs = await getResponsesForGreivance(grievance_key);
  await Promise.all(
    grs.map(async (res) => {
      await deletedDB.put({ ...gr }, `deleted_response_${res.key}`);
      await responsesDB.delete(res.key);
      await newLog(LogType.DELETE_RESPONSE, {id: res.key}, res.userId, gr.type);
    })
  );
  return true
}

export async function createNewResponse(response: ResponseInput) {
  return responsesDB.put({
    ...response,
    key: (9000000000000 - Date.now()).toString(),
    createdAt: Date.now(),
  }) as unknown as GResponseType | null;
}

export async function getResponsesForGreivance(grievanceId: string) {
  const responses = await responsesDB.fetch({ grievanceId });
  if (responses.count < 1) return [];
  return responses.items as unknown as GResponseType[];
}

export async function getResponse(responseId: string) {
  return responsesDB.get(responseId) as unknown as GResponseType | null;
}

export async function deleteResponse(responseId:string) {
  const response = await getResponse(responseId);
  if (!response) return null;
  await deletedDB.put({ ...response }, `deleted_response_${response.key}`);
  await responsesDB.delete(responseId);
  return true
}