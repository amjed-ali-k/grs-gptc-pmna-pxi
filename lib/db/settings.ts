import {
  AuthSettings,
  EmailSettings,
  FrontPageSettings,
  InstitutionSettings,
  IntegrationSettings,
  InternalGrievanceSettings,
  OAuthSettings,
  SettingsType,
  StaffGrievanceSettings,
  StudentGrievanceSettings,
} from "../constants/settings";
import { settingsDB } from "./db";
import cache from "memory-cache";

var settingsCache = new cache.Cache();

export async function getSettings<T>(type: SettingsType) {
  let settings;
  const csettings = settingsCache.get(type);
  if (csettings) {
    settings = csettings;
  } else {
    settings = await settingsDB.get(type);
  }
  if (!settings) {
    settings = await populateSettings(type);
  }
  return typed(type, settings) as unknown as T;
}

async function populateSettings(type: SettingsType) {

  const defaultPriorities  = [
    {
      name: "Very High",
      value: "very-high",
    },
    {
      name: "High",
      value: "high",
    },
    {
      name: "Medium",
      value: "medium",
    },
    {
      name: "Low",
      value: "low",
    },
    {
      name: "Very Low",
      value: "very-low",
    },
  ]

  const defaultStatuses = [
    {
      name: "Open",
      value: "open",

    },
    {
      name: "Closed",
      value: "closed",
    },
    {
      name: "Resolved",
      value: "resolved",
    },

  ]

  const defaultCategories = [
    {
      name: "Academic Issues",
      value: "cuVdxo",
      active: true,
    },
    {
      name: "Administrative Issues",
      value: "0m6lDe",
      active: true,
    }
  ]

  const defaultSettings = {
    staff: {
      enabled: true,
      contentTitle: "Grievance Content",
      responsesTitle: "Responses",
      infoCardTitle: "Grievance Info",
      showUserCard: true,
      banner: "Here you can view and respond to grievance",
      showDate: true,
      showTime: true,
      showPriority: true,
      showStatus: true,
      showCategory: true,
      showIpAddress: true,
      contentLengthMax: 1000,
      contentLengthMin: 10,
      responsesLengthMax: 500,
      responsesLengthMin: 5,
      allowImageUpload: false,
      allowAuthorResponse: true,
      allowedPriorities: defaultPriorities,
      allowedStatuses: defaultStatuses,
      allowedCategories: defaultCategories,
    },
    student: {
      enabled: true,
      contentTitle: "Grievance Content",
      responsesTitle: "Responses",
      infoCardTitle: "Grievance Info",
      showUserCard: true,
      banner: "Here you can view and respond to grievance",
      showDate: true,
      showTime: true,
      showPriority: true,
      showStatus: true,
      showCategory: true,
      showIpAddress: true,
      contentLengthMax: 1000,
      contentLengthMin: 10,
      responsesLengthMax: 500,
      responsesLengthMin: 5,
      allowImageUpload: false,
      allowAuthorResponse: true,
      allowedPriorities: defaultPriorities,
      allowedStatuses: defaultStatuses,
      allowedCategories: defaultCategories,
    },
    internal: {
      enabled: true,
      contentTitle: "Grievance Content",
      responsesTitle: "Responses",
      infoCardTitle: "Grievance Info",
      showUserCard: true,
      banner: "Here you can view and respond to grievance",
      showDate: true,
      showTime: true,
      showPriority: true,
      showStatus: true,
      showCategory: true,
      showIpAddress: true,
      contentLengthMax: 1000,
      contentLengthMin: 10,
      responsesLengthMax: 500,
      responsesLengthMin: 5,
      allowImageUpload: false,
      allowAuthorResponse: true,
      allowedPriorities: defaultPriorities,
      allowedStatuses: defaultStatuses,
      allowedCategories: defaultCategories,
    },
    institution: {
      name: "",
      logo: "",
      logoUrl: "",
      about: "",
      address: "",
      phone: "",
      email: "",
      website: "",
    },
    email: {
      enabled: true,
      host: "",
      port: 465,
      username: "",
      password: "",
      from: "",
      fromName: "",
      registrationEnabled: true,
      registrationSubject: "",
      loginEnabled: true,
      loginSubject: "",
      resetPasswordEnabled: true,
      resetPasswordSubject: "",
    },
    oauth: {
      allowPasswordLogin: true,
      google: {
        enabled: false,
        clientId: "",
        clientSecret: "",
      },
      facebook: {
        enabled: false,
        clientId: "",
        clientSecret: "",
      },
      twitter: {
        enabled: false,
        clientId: "",
        clientSecret: "",
      },
      github: {
        enabled: false,
        clientId: "",
        clientSecret: "",
      },
      linkedin: {
        enabled: false,
        clientId: "",
        clientSecret: "",
      },
    },
    auth: {
      allowLogin: true,
      allowRegistration: true,
      allowResetPassword: true,
      allowChangePassword: true,
      enableRecaptcha: false,
      recaptchaSiteKey: "",
      passwordMinLength: 8,
      passwordMaxLength: 20,
      passwordUppercaseLetter: false,
      passwordLowercaseLetter: false,
      passwordNumber: false,
      passwordSpecialCharacter: false,
      userEmailVerification: false,
      blacklistedDomains: [],
      whiteListedDomains: [],
    },
    frontpage: {
      enabled: true,
      title: "",
      description: "",
      image: "",
      backgroundImage: "",
      analyticsTrackingId: "",
      analyticsEnabled: false,
      showContactDetails: true,
      showStaffGrievanceCellMembers: true,
      showStudentGrievanceCellMembers: true,
      showInternalComplaintsCellMembers: true,
      showInstitutionLogo: true,
    },
    integration: {
      whatsapp: {
        enabled: false,
        groupId: "",
        number: "",
      },
      telegram: {
        enabled: false,
        botToken: "",
        groupId: "",
      },
      sms: {
        enabled: false,
        gatewayUrl: "",
        gatewayUsername: "",
        gatewayPassword: "",
        gatewaySenderId: "",
        gatewaySenderName: "",
        gatewaySenderNumber: "",
        gatewaySenderCountryCode: "",
      },
    },
  };
  return settingsDB.put(defaultSettings[type], type);
}

function typed(type: SettingsType, settings: any) {
  switch (type) {
    case SettingsType.StaffGrievance:
      return settings as unknown as StaffGrievanceSettings;
    case SettingsType.StudentGrievance:
      return settings as unknown as StudentGrievanceSettings;
    case SettingsType.InternalGrievance:
      return settings as unknown as InternalGrievanceSettings;
    case SettingsType.Institution:
      return settings as unknown as InstitutionSettings;
    case SettingsType.Email:
      return settings as unknown as EmailSettings;
    case SettingsType.OAuth:
      return settings as unknown as OAuthSettings;
    case SettingsType.Auth:
      return settings as unknown as AuthSettings;
    case SettingsType.FrontPage:
      return settings as unknown as FrontPageSettings;
    case SettingsType.Integration:
      return settings as unknown as IntegrationSettings;
    default:
      return null;
  }
}

export async function updateSettings(type: SettingsType, settings: any) {
  settingsCache.del(type)
  const _settings = await settingsDB.get(type);
  const set = settingsDB.put({ ..._settings, ...settings }, type);
  settingsCache.put(type, set);
  return typed(type, set);
}
