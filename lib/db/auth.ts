import { accountsDB, deletedDB, sessionDB, tokenDB, usersDB } from "./db";
import type { Account } from "next-auth";
import type {
  AdapterSession,
  VerificationToken,
  AdapterUser,
} from "next-auth/adapters";
import ShortUniqueId from "short-unique-id";
import cache from "memory-cache";

var usersCache = new cache.Cache();
const uid = new ShortUniqueId();

interface Session extends AdapterSession {
  key?: string;
}
interface VT extends VerificationToken {
  key?: string;
}
interface KeyedAccount extends Account {
  key?: string;
}

export async function createUser(user: AdapterUser) {
  const id = uid.stamp(32);
  if (!user || !user.email) return null;
  const _user = {
    ...user,
    active: true,
    createdAt: Date.now(),
    updatedAt: Date.now(),
  };
  return usersDB.put({
    ..._user,
    key: id,
    id,
  } as {}) as unknown as AdapterUser | null;
}

export async function updateUser(user: any) {
  const currenUser = await usersDB.get(user.id);
  const _user = (await usersDB.put({
    ...currenUser,
    ...user,
    updatedAt: Date.now(),
  } as {})) as unknown as AdapterUser | null;
  usersCache.put(user.id, _user);
  return _user;
}

export async function deleteUser(user?: AdapterUser, key?: string) {
  let _key = null;
  _key = user ? user.id : key;
  if (!_key) return null;
  const currenUser = await usersDB.get(_key);
  await deletedDB.put({ ...currenUser, key: `user_${_key}` });
  await usersDB.delete(_key);
  return usersDB.delete(_key);
}

export async function getUser(id?: string) {
  if (!id) return null;
  const user = usersCache.get(id) as AdapterUser | null;
  if (user) return user;
  const _user = (await usersDB.get(id)) as unknown as AdapterUser | null;
  if (!_user) return null;
  usersCache.put(id, _user);
  return _user;
}

export async function getUserByEmail(email: string) {
  const user = usersCache.get(email) as AdapterUser | null;
  if (user) return user;
  const _user = (await usersDB.fetch({ email }))
    .items[0] as unknown as AdapterUser | null;
  if (!_user) return null;
  usersCache.put(email, _user);
  return _user;
}

export async function getUserByAccount(account_id: string) {
  if (!account_id) return null;
  const account = (await accountsDB.get(account_id)) as unknown as KeyedAccount;
  if (!account) return null;
  return usersDB.get(account.userId) as unknown as AdapterUser | null;
}

export async function linkAccount(data: KeyedAccount) {
  const _data = { ...data, key: data.providerAccountId };
  return accountsDB.put(_data) as unknown as Account;
}

export async function unlinkAccount(account_id: string) {
  await accountsDB.delete(account_id);
  return account_id;
}

export async function createSession(session: Session) {
  if (!("key" in session)) session.key = session.sessionToken;
  return sessionDB.put(session as {}) as unknown as Session;
}

export async function updateSession(session: Session) {
  if (!("key" in session)) session.key = session.sessionToken;
  return sessionDB.put(session as {});
}

export async function deleteSession(sessionToken: Session["sessionToken"]) {
  return sessionDB.delete(sessionToken);
}

export async function getSessionAndUser(sessionToken: Session["sessionToken"]) {
  const session = (await sessionDB.get(
    sessionToken
  )) as unknown as Session | null;
  if (!session) return null;
  const user = (await usersDB.fetch({ id: session.userId }))
    .items[0] as unknown as AdapterUser | null;
  if (!user) return null;
  return { user, session: fromDB(session) };
}

export async function createVT(token: VT) {
  if (!token?.key) token.key = token.identifier;
  return tokenDB.put(token as {}) as unknown as VT | null;
}

export async function useVT(key: string) {
  const token = (await tokenDB.get(key)) as unknown as VT | null;
  if (!token) return null;
  delete token.key;
  return token;
}

function fromDB(object: Session): Session {
  object.expires = new Date(
    Date.parse(object.expires as unknown as string)
  ).getTime() as any;
  return object;
}
