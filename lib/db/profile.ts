import { AuthUserType, ProfileType, UserType } from "../types/db";
import { compare } from "bcryptjs";
import { createUser, getUser, getUserByEmail } from "./auth";
import merge from "lodash.merge";
import cache from "memory-cache";
import ShortUniqueId from "short-unique-id";
import { profileDB } from "./db";

const uid = new ShortUniqueId();
var profileCache = new cache.Cache();

export async function verifyLogin(email?: string, password?: string) {
  // TODO: Implement Brute Force Lock
  if (!email || !password) return false;
  const profile = await getProfileByEmail(email);
  if (!profile || !profile.password) return false;
  const user = await getUser(profile.key);
  if (!user || !user.active) return false;
  const isValid = await compare(password, profile?.password);
  if (!isValid) return false;
  return user;
}

export async function getProfileByEmail(email: string) {
  let profile = profileCache.get(email);
  if (profile) return profile as UserType;
  profile = (await profileDB.fetch({ email })).items[0];
  if (!profile) return null;
  profileCache.put(email, profile);
  return profile as UserType | null;
}

export async function createNewProfile(user: UserType | ProfileType) {
  profileCache.del(user.email);
  profileCache.del(user.key);
  if (!user.key) user.key = (await getUserByEmail(user.email))?.key as string;
  if (!user.key) throw new Error("User not found");
  const profile = await getProfileById(user.key);
  if (profile) return null;
  return profileDB.put(user as {}, user.key) as unknown as UserType | null;
}

export async function getProfileById(id: string) {
  let profile = profileCache.get(id) as UserType | null;
  if (profile) return profile;
  profile = (await profileDB.get(id)) as unknown as UserType | null;
  if (!profile) return null;
  profileCache.put(id, profile);
  return profile;
}

export async function updateProfile(user: ProfileType, key?: string) {
  const euser = await getProfileById(key || user.key);
  if (!euser) return null;
  profileCache.del(euser.key);
  profileCache.del(euser.email);
  const _user = await getUser(user.key);
  if (_user) profileCache.del(_user.email);

  return profileDB.put(
    merge(euser, user) as {},
    user.key
  ) as unknown as UserType | null;
}

export async function changePassword(user: UserType, password: string) {
  profileDB.put(merge(user, { password }) as {});
}

export async function createNewUser(user: UserType & AuthUserType) {
  const _user = await createUser({
    id: "",
    email: user.email,
    name: user.name,
    emailVerified: null,
    image: user.image ? user.image : null,
  });
  if (!_user) return null;
  return profileDB.put({ ...user } as {}, _user.id);
}

export async function getAllUsersWithProfile() {
  let usersdb = (await profileDB.fetch({}))
  let users = usersdb.items as unknown as ProfileType[]
  while(usersdb.last){
    usersdb = await profileDB.fetch({}, {last: usersdb.last});
    users = users.concat(usersdb.items as unknown as ProfileType[]);
  }
  return Promise.all(
    users.map(async (user) => {
      const _u = await getUser(user.key);
      return {
        ..._u,
        ...user,
      };
    })
  );
}
