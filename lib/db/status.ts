import { AdapterUser } from "next-auth/adapters";
import { GrievanceType } from "../types/db";
import { grievanceDB } from "./db";

export async function getGrievanceCountByStatusByUser(user: AdapterUser) {
  const result = await grievanceDB.fetch({ userId: user.id });
  if (result.count === 0) {
    return {
      processing: 0,
      solved: 0,
      rejected: 0,
      total: 0,
    };
  }
  const grievances = result.items as unknown as GrievanceType[];
  const processing = grievances.filter(
    (grievance) => grievance.status === "processing"
  ).length;
  const solved = grievances.filter(
    (grievance) => grievance.status === "solved"
  ).length;
  const rejected = grievances.filter(
    (grievance) => grievance.status === "rejected"
  ).length;
  const total = grievances.length;
  return {
    processing,
    solved,
    rejected,
    total: total,
  };
}
