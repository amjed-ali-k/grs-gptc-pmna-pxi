import { GrTypeType, Statuses } from "../constants/grievance";
import { logsDB } from "./db";

export enum LogType {
  NEW_USER = "new_user",
  NEW_ACCOUNT = "new_account",
  NEW_SESSION = "new_session",
  NEW_LOGIN = "new_login",
  NEW_GRIEVANCE = "new_grievance",
  NEW_RESPONSE = "new_response",
  CHANGE_STATUS = "change_status",
  CHANGE_PRIORITY = "change_priority",
  DELETE_GRIEVANCE = "delete_grievance",
  DELETE_RESPONSE = "delete_response",
  DELETE_USER = "delete_user",
}

export async function newLog(type: LogType, data: any, userId: string, grType?: GrTypeType) {
  return logsDB.put(
    { data, type, userId, createAt: new Date().getTime(), grType: grType || null  },
    (9000000000000 - new Date().getTime()).toString()
  ) as unknown as BasicLog | null;
}

export interface BasicLog {
  type: LogType;
  grsType: GrTypeType;
  data: any;
  userId: string;
  createAt: number;
}

export interface NewGrievanceLog extends BasicLog {
  data: {
    ip: string;
    type: GrTypeType;
    id: string;
    category: string;
    department: string;
    priority: string;
  };
}

export interface NewResponseLog extends BasicLog {
  data: {
    id: string;
    status: Statuses;
  };
}

export interface ChangeStatusLog extends BasicLog {
  data: {
    from: Statuses;
    to: Statuses;
    by: string;
    grievanceId: string;
    grievanceCategory: string;
    authorDepartment: string;
    grievancePriority: string;
  };
}


export type LogDBType = NewGrievanceLog | NewResponseLog | ChangeStatusLog;