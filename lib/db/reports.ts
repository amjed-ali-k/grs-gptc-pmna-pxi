import _d from "dayjs";
import { ClosedStatuses, GrTypeType, Statuses } from "../constants/grievance";
import { logsDB, reportsDB } from "./db";
import { ChangeStatusLog, LogType, NewGrievanceLog } from "./logs";

export interface GrsReport {
  key: string;
  totalNewGrievances: number;
  totalGrievancesCarriedForward?: {
    [key in Statuses]?: number;
  };
  totalGrievancesBroughtForward?: {
    [key in Statuses]?: number;
  };
  totalGrievancesByStatus?: {
    [key in Statuses]?: number;
  };
  totalGrievancesOpenedByDepartment?: {
    [key: string]: number;
  };
  totalGrievancesOpenedByPriority?: {
    [key: string]: number;
  };
  totalGrievancesOpenedByCategory?: {
    [key: string]: number;
  };
  totalGrievancesClosedByPriority?: {
    [key: string]: number;
  };
  totalGrievancesClosedByCategory?: {
    [key: string]: number;
  };
  totalGrievancesClosedByDepartment?: {
    [key: string]: number;
  };
  totalGrievancesClosed: number;
  finalized: boolean;
  month: number;
  year: number;
  type: GrTypeType;
}

export async function generateAndSaveMonthlyReport(
  month: number,
  year: number,
  type: GrTypeType
) {
  const rep = await generateMontlyReport(month, year, type);
  if (rep) await reportsDB.put({ ...rep });
  return rep;
}

export async function generateMontlyReport(
  month: number,
  year: number,
  type: GrTypeType
) {
  console.log(`Requested report for ${month}/${year} - ${type}`);
  const _existing = await getReport(month, year, type);
  if (_existing) console.log(`Report Exists in DB`);
  if (_existing && _existing.finalized) return _existing;
  if (_existing) console.log(`But not finalized`);
  if (_existing === false) return false;
  console.log(`Generating report for ${month}/${year} - ${type}`);
  let report: GrsReport = {
    key: `${year}-${month}-${type}`,
    finalized: false,
    month,
    year,
    type,
    totalGrievancesClosed: 0,
    totalNewGrievances: 0,
  };
  const newGrsLogs = await getAllGrsLogsInMonth<NewGrievanceLog>(
    month,
    year,
    LogType.NEW_GRIEVANCE,
    type
  );
  report.totalGrievancesByStatus = {};
  report.totalNewGrievances = newGrsLogs.length;
  console.log(`New grievances: ${newGrsLogs.length}`);
  for (const log of newGrsLogs) {
    const { category, department, priority } = log.data;
    let {
      totalGrievancesOpenedByCategory,
      totalGrievancesOpenedByPriority,
      totalGrievancesOpenedByDepartment,
    } = report;
    if (!totalGrievancesOpenedByCategory) totalGrievancesOpenedByCategory = {};
    if (!totalGrievancesOpenedByPriority) totalGrievancesOpenedByPriority = {};
    if (!totalGrievancesOpenedByDepartment)
      totalGrievancesOpenedByDepartment = {};
    totalGrievancesOpenedByCategory = {
      ...totalGrievancesOpenedByCategory,
      [category]: (totalGrievancesOpenedByCategory[category] || 0) + 1,
    };
    totalGrievancesOpenedByPriority = {
      ...totalGrievancesOpenedByPriority,
      [priority]: (totalGrievancesOpenedByPriority[priority] || 0) + 1,
    };
    totalGrievancesOpenedByDepartment = {
      ...totalGrievancesOpenedByDepartment,
      [department]: (totalGrievancesOpenedByDepartment[department] || 0) + 1,
    };
    report.totalGrievancesOpenedByCategory = totalGrievancesOpenedByCategory;
    report.totalGrievancesOpenedByPriority = totalGrievancesOpenedByPriority;
    report.totalGrievancesOpenedByDepartment =
      totalGrievancesOpenedByDepartment;
  }
  console.log(
    `Opened grievances by category: ${JSON.stringify(
      report.totalGrievancesOpenedByCategory
    )}`
  );
  const changeStatusLogs = await getAllGrsLogsInMonth<ChangeStatusLog>(
    month,
    year,
    LogType.CHANGE_STATUS,
    type
  );
  for (const log of changeStatusLogs) {
    // Dont Repeat same GRSs
    let grievanceIds: string[] = [];
    if (grievanceIds.includes(log.data.grievanceId)) {
      continue;
    }
    grievanceIds.push(log.data.grievanceId);

    const { to, authorDepartment, grievancePriority, grievanceCategory } =
      log.data;
    if (!report.totalGrievancesByStatus) report.totalGrievancesByStatus = {};
    let totalGrievancesByStatus = report.totalGrievancesByStatus[to] || 0;
    totalGrievancesByStatus++;
    report.totalGrievancesByStatus[to] = totalGrievancesByStatus;

    if (!ClosedStatuses.includes(to)) continue;
    report.totalGrievancesClosed++;
    if (!report.totalGrievancesClosedByDepartment)
      report.totalGrievancesClosedByDepartment = {};
    report.totalGrievancesClosedByDepartment = {
      ...report.totalGrievancesClosedByDepartment,
      [authorDepartment]:
        (report.totalGrievancesClosedByDepartment[authorDepartment] || 0) + 1,
    };
    if (!report.totalGrievancesClosedByPriority)
      report.totalGrievancesClosedByPriority = {};
    report.totalGrievancesClosedByPriority = {
      ...report.totalGrievancesClosedByPriority,
      [grievancePriority]:
        (report.totalGrievancesClosedByPriority[grievancePriority] || 0) + 1,
    };
    if (!report.totalGrievancesClosedByCategory)
      report.totalGrievancesClosedByCategory = {};
    report.totalGrievancesClosedByCategory = {
      ...report.totalGrievancesClosedByCategory,
      [grievanceCategory]:
        (report.totalGrievancesClosedByCategory[grievanceCategory] || 0) + 1,
    };
  }
  console.log(
    `Closed grievances by category: ${JSON.stringify(
      report.totalGrievancesClosedByCategory
    )}`
  );

  const requestedTime = new Date(year, month);
  if (
    new Date().getMonth() < requestedTime.getMonth() &&
    new Date().getFullYear() < requestedTime.getFullYear()
  ) {
    report.finalized = true;
  }

  // Carried and Brought Forward
  const prevReport = await generateAndSaveMonthlyReport(
    new Date(year, month - 1).getMonth(),
    new Date(year, month - 1).getFullYear(),
    type
  );

  if (!report.totalGrievancesCarriedForward)
    report.totalGrievancesCarriedForward = {};

  for (const status of Object.values(Statuses)) {
    report.totalGrievancesCarriedForward = {
      ...report.totalGrievancesCarriedForward,
      [status]:
        prevReport === false
          ? 0
          : prevReport.totalGrievancesCarriedForward
          ? prevReport.totalGrievancesCarriedForward[status]
          : 0,
    };

    let {
      totalGrievancesBroughtForward,
      totalGrievancesCarriedForward,
      totalGrievancesByStatus,
    } = report;
    if (!totalGrievancesBroughtForward) totalGrievancesBroughtForward = {};
    totalGrievancesBroughtForward = {
      ...totalGrievancesBroughtForward,
      [status]:
        (totalGrievancesCarriedForward[status] || 0) +
        (totalGrievancesByStatus[status] || 0),
    };
    report.totalGrievancesBroughtForward = totalGrievancesBroughtForward;
  }
  return report;
}

async function getAllGrsLogsInMonth<T>(
  month: number,
  year: number,
  type: LogType,
  grType: GrTypeType
) {
  const start = _d(`${year}-${month}-01`).startOf("month").toDate().getTime();
  const end = _d(`${year}-${month}-01`).endOf("month").toDate().getTime();
  console.log(
    "Fetching logs from",
    start,
    "to",
    end, "of type", grType, "in log", type
  );
  return (
    await logsDB.fetch({
      "createAt?r": [start, end],
      type,
      grType,
    })
  ).items as unknown as T[]; 
}

export async function getReport(month: number, year: number, type: GrTypeType) {
  // TODO: Get System Initialized Date from SETTINGS.
  const time = 1640975400000; // Jan 01 2022
  const pdefMonth = new Date(time).getMonth();
  const pdefYear = new Date(time).getFullYear();
  const requestedTime = new Date(year, month).getTime();
  const requestedMonth = new Date(requestedTime).getMonth();
  const requestedYear = new Date(requestedTime).getFullYear();

  if (requestedMonth < pdefMonth || requestedYear < pdefYear) {
    return false;
  }
  return reportsDB.get(
    `${requestedYear}-${requestedMonth}-${type}`
  ) as unknown as GrsReport | null;
}
