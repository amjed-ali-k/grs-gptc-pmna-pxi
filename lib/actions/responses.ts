import { LogType, newLog } from "../db/logs";
import { GResponseType, GrievanceType, UserWithProfile } from "../types/db";

export async function newResponseAction(
  response: GResponseType,
  grievance: GrievanceType,
  user: UserWithProfile
) {
  await newLog(
    LogType.NEW_RESPONSE,
    { id: response.key, status: response.status },
    response.userId,
    grievance.type
  );
  if (grievance.status !== response.status)
    await newLog(
      LogType.CHANGE_STATUS,
      {
        from: grievance.status,
        to: response.status,
        by: user.key,
        grievanceId: grievance.key,
        grievanceCategory: grievance.category,
        authorDepartment: user.department,
        grievancePriority: grievance.priority,
      },
      user.key,
      grievance.type
    );
}
