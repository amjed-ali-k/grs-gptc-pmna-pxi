import { GrTypeType } from "./grievance";

export type GrievanceSettings = {
  enabled: boolean;
  contentTitle: string;
  responsesTitle: string;
  infoCardTitle: string;
  showUserCard: boolean;
  banner: string;
  showDate: boolean;
  showTime: boolean;
  showPriority: boolean;
  showStatus: boolean;
  showCategory: boolean;
  showIpAddress: boolean;
  // ==============================
  contentLengthMax: number;
  contentLengthMin: number;
  responsesLengthMax: number;
  responsesLengthMin: number;
  allowImageUpload: boolean;
  allowAuthorResponse: boolean;
  allowedPriorities: {
    name: string;
    value: string;
  }[];
  allowedStatuses: {
    name: string;
    value: string;
  }[];
  allowedCategories: {
    name: string;
    value: string;
  }[];
};

export interface StaffGrievanceSettings extends GrievanceSettings {
  type: GrTypeType.STAFF;
  key: SettingsType.StaffGrievance;
}

export interface StudentGrievanceSettings extends GrievanceSettings {
  type: GrTypeType.STUDENT;
  key: SettingsType.StudentGrievance;
}

export interface InternalGrievanceSettings extends GrievanceSettings {
  type: GrTypeType.INTERNAL;
  key: SettingsType.InternalGrievance;
}

export type InstitutionSettings = {
  key: SettingsType.Institution;
  name: string;
  logo: string;
  logoUrl: string;
  about: string;
  address: string;
  phone: string;
  email: string;
  website: string;
};

export type EmailSettings = {
  key: SettingsType.Email;
  enabled: boolean;
  host: string;
  port: number;
  username: string;
  password: string;
  fromAddress: string;
  fromName: string;
  registrationEnabled: boolean;
  registrationSubject: string;
  loginEnabled: boolean;
  loginSubject: string;
  resetPasswordEnabled: boolean;
  resetPasswordSubject: string;
};

export type OAuthSettings = {
  key: SettingsType.OAuth;
  allowPasswordLogin: boolean;
  google: {
    enabled: boolean;
    clientId: string;
    clientSecret: string;
  };
  facebook: {
    enabled: boolean;
    clientId: string;
    clientSecret: string;
  };
  twitter: {
    enabled: boolean;
    clientId: string;
    clientSecret: string;
  };
  github: {
    enabled: boolean;
    clientId: string;
    clientSecret: string;
  };
  linkedin: {
    enabled: boolean;
    clientId: string;
    clientSecret: string;
  };
};

export type IntegrationSettings = {
  key: SettingsType.Integration;
  whatsapp: {
    enabled: boolean;
    groupId: string;
    number: string;
  };
  telegram: {
    enabled: boolean;
    botToken: string;
    groupId: string;
  };
  sms: {
    enabled: boolean;
    gatewayUrl: string;
    gatewayUsername: string;
    gatewayPassword: string;
    gatewaySenderId: string;
    gatewaySenderName: string;
    gatewaySenderNumber: string;
    gatewaySenderCountryCode: string;
  };
};

export type AuthSettings = {
  key: SettingsType.Auth;
  allowRegistration: boolean;
  allowResetPassword: boolean;
  allowChangePassword: boolean;
  enableRecaptcha: boolean;
  recaptchaSiteKey: string;
  passwordMinLength: number;
  passwordMaxLength: number;
  passwordUppercaseLetter: boolean;
  passwordLowercaseLetter: boolean;
  passwordNumber: boolean;
  passwordSpecialCharacter: boolean;
  userEmailVerification: boolean;
  blacklistedDomains: string[];
  whiteListedDomains: string[];
  semesters : string[];
  designations: string[];
  departments: string[];
};

export type FrontPageSettings = {
  key: SettingsType.FrontPage;
  enabled: boolean;
  title: string;
  description: string;
  image: string;
  backgroundImage: string;
  analyticsTrackingId: string;
  analyticsEnabled: boolean;
  showContactDetails: boolean;
  showStaffGrievanceCellMembers: boolean;
  showStudentGrievanceCellMembers: boolean;
  showInternalComplaintsCellMembers: boolean;
  showInstitutionLogo: boolean;
};

export enum SettingsType {
  StaffGrievance = "staff",
  StudentGrievance = "student",
  InternalGrievance = "internal",
  Institution = "institution",
  Email = "email",
  OAuth = "oauth",
  Integration = "integration",
  Auth = "auth",
  FrontPage = "frontpage",
}
