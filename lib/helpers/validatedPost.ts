import { NextApiRequest, NextApiResponse } from "next";
import { AnyObjectSchema } from "yup";
import { validation } from "./validation";


export default function validatedPost (
  schema: AnyObjectSchema | ((req: NextApiRequest) => Promise<AnyObjectSchema>),
  callback: (
    data: any,
    req: NextApiRequest,
    res: NextApiResponse<{} | { error: string }>
  ) => Promise< any>
) {
  return async function handler(
    req: NextApiRequest,
    res: NextApiResponse<{} | { error: string }>
  ) {
    if (req.method === "POST") {
      const _schema = typeof schema === "function" ? await schema(req) : schema;
      const { isValid, errors, data } = await validation(_schema, req.body);
      if (!isValid) {
        return res.status(400).json({ error: errors });
      }
      try {
        await callback(data, req, res);
        return res.status(200).json({
          message: "success",
          data,
        });
      } catch (error: any) {
        return res.status(400).json({ error: error.message });
      }
      
    } else {
      return res.status(403).json({
        message: "method not allowed",
      });
    }
  };
}
