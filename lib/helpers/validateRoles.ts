import { getSession } from "next-auth/react";
import { getRoleByEmail, getRoleByEmailWithPermissions } from "../db/roles";

export async function validateRole(roles: string[], req: any, email?: string) {
  const _email = email ? email : await getEmail(req);
  if (!_email) return null;
  const _roles = await getRoleByEmail(email || _email);
  if (!_roles || !_roles.roles || !_roles.roles.some((r) => roles.includes(r)))
    return false;
  return true;
}


export async function validatePermissions(email:string, permissions: string[]) {
  const rl_pms = await getRoleByEmailWithPermissions(email);
  if (!rl_pms || !rl_pms.permissions || !rl_pms.permissions.some((p) => permissions.includes(p)))
    return false;
  return true;
}


export async function getEmail(req?: any) {
  const session = await getSession(req ? { req } : undefined);
  if (
    !session ||
    !session.user ||
    !session.user.email ||
    session.user.email === undefined ||
    session.user.email === null
  )
    return null;
  return session.user.email;
}
