import { NextApiRequest, NextApiResponse } from "next";
import { APIError } from "../constants/users";
export default function backendGet(
  callback: (req: NextApiRequest) => Promise<any>
) {
  return async function handler(req: NextApiRequest, res: NextApiResponse<{}>) {
    if (req.method === "GET") {
      try {
        const data = await callback(req);
        if (!data) return res.status(204).json({ message: "No response" });
        return res.status(200).json(data);
      } catch (error: any) {
        if (error.message === APIError.UNAUTHORIZED) {
          return res
            .status(401)
            .json({ error: "You have to login to view this content." });
        }
        if (error.message === APIError.NO_PROFILE) {
          return res
            .status(406)
            .json({
              error: "You have to complete your profile to view this content.",
            });
        }
        return res.status(500).json({ error: error.message });
      }
    } else {
      return res.status(403).json({
        message: "method not allowed",
      });
    }
  };
}
