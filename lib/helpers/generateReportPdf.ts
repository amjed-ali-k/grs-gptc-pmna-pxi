import _d from "dayjs";

import { GrsReport } from "../db/reports";
import { Statuses } from "../constants/grievance";
import { GResponseType, GrievanceType, UserType } from "../types/db";

const styles = {
  header: {
    fontSize: 18,
    bold: true,
    margin: [0, 0, 0, 10],
  },
  subheader: {
    fontSize: 14,
    bold: true,
    margin: [0, 10, 0, 5],
  },
  fontXs: {
    fontSize: 10,
  },
  headerRight: {
    alignment: "right",
  },
  tableHeader: {
    bold: true,
    fillColor: "#eeeeee",
  },
  bold: {
    bold: true,
  },
  boldItalic: {
    bold: true,
    italics: true,
  },
  tableExample: {
    margin: [0, 5, 0, 15],
    bold: true,
  },
  footerText: {
    margin: [0, 20, 0, 0],
    alignment: "center",
    fontSize: 10,
  },
};

export function generateReportPdf(data: GrsReport) {
  let definition: any = {};
  definition.styles = styles;

  let content = [];
  content.push({ text: "GRS Report", style: "header" });
  const formatedDate = _d(new Date()).format("D-MMM-YYYY hh:mm A");
  content.push({
    columns: [
      {
        text: [
          { text: `Generated on ${formatedDate}` },
          "\n",
          "www.pixiegrs.com",
          "\n",
          "",
        ],
        width: "50%",
        style: "fontXs",
      },
      {
        text: [
          `Report of ${data.month} ${data.year}`,
          "\n",
          {
            text: `${data.type.toLocaleUpperCase()} Grievances`,
            style: "bold",
            fontSize: 14,
          },
          "\n",
          { text: "Monthly Report", fontSize: 11, style: "boldItalic" },
        ],
        width: "50%",
        style: "headerRight",
      },
    ],
  });
  content.push({ text: "Basic Report", style: "subheader" });
  content.push({
    table: {
      widths: ["*", "auto"],
      style: "tableExample",
      body: [
        [
          "Total new grievances this month",
          { text: `${data.totalNewGrievances}`, alignment: "right" },
        ],
        [
          "Total grievances closed this month",
          { text: `${data.totalGrievancesClosed}`, alignment: "right" },
        ],
        [
          { text: "Remaining Greivances", bold: true },
          {
            text: `${data.totalNewGrievances - data.totalGrievancesClosed}`,
            alignment: "right",
          },
        ],
      ],
    },
  });
  content.push({ text: "Grievances from ", style: "subheader" });
  content.push({
    table: {
      widths: ["*", "auto", "auto"],
      style: "tableExample",
      body: [
        [
          { text: "Type", style: "tableHeader" },
          { text: "Open", style: "tableHeader" },
          { text: "Processing", style: "tableHeader" },
        ],
        [
          "Total grievances Carried Forward from previous month",
          {
            text: `${data.totalGrievancesCarriedForward?.open || 0}`,
            alignment: "right",
          },
          {
            text: `${
              (data.totalGrievancesCarriedForward?.solved || 0) +
              (data.totalGrievancesCarriedForward?.rejected || 0) +
              (data.totalGrievancesCarriedForward?.duplicate || 0) +
              (data.totalGrievancesCarriedForward?.fake || 0) +
              (data.totalGrievancesCarriedForward?.less_info || 0)
            }`,
            alignment: "right",
          },
        ],
        [
          "Total grievances Brought Forward to next month",
          {
            text: `${data.totalGrievancesBroughtForward?.open || 0}`,
            alignment: "right",
          },
          {
            text: `${
              (data.totalGrievancesBroughtForward?.solved || 0) +
              (data.totalGrievancesBroughtForward?.rejected || 0) +
              (data.totalGrievancesBroughtForward?.duplicate || 0) +
              (data.totalGrievancesBroughtForward?.fake || 0) +
              (data.totalGrievancesBroughtForward?.less_info || 0)
            }`,
            alignment: "right",
          },
        ],
      ],
    },
  });
  content.push({ text: "Department wise report", style: "subheader" });
  const drContainer: any = {
    table: {
      widths: ["*", "auto", "auto"],
      style: "tableExample",
      body: [
        [
          { text: "Department", style: "tableHeader" },
          { text: "Open", style: "tableHeader" },
          //   { text: "Processing", style: "tableHeader" },
          { text: "Closed", style: "tableHeader" },
        ],
      ],
    },
  };
  const allDepts = new Set([
    ...Object.keys(data.totalGrievancesClosedByDepartment || {}),
    ...Object.keys(data.totalGrievancesOpenedByDepartment || {}),
  ]);
  allDepts.forEach((dept) => {
    const closed = data.totalGrievancesClosedByDepartment
      ? data.totalGrievancesClosedByDepartment[dept.toString()]
      : 0;
    const open = data.totalGrievancesOpenedByDepartment
      ? data.totalGrievancesOpenedByDepartment[dept.toString()]
      : 0;
    drContainer.table.body.push([
      `${dept.toString()}`,
      { text: `${open || 0}`, alignment: "right" },
      { text: `${closed || 0}`, alignment: "right" },
    ]);
  });
  content.push(drContainer);
  content.push({ text: "Category wise report", style: "subheader" });
  const ctContainer: any = {
    table: {
      widths: ["*", "auto", "auto"],
      style: "tableExample",
      body: [
        [
          { text: "Category Name", style: "tableHeader" },
          { text: "Open", style: "tableHeader" },
          { text: "Closed", style: "tableHeader" },
        ],
      ],
    },
  };
  const allCats = new Set([
    ...Object.keys(data.totalGrievancesOpenedByCategory || {}),
    ...Object.keys(data.totalGrievancesClosedByCategory || {}),
  ]);
  allCats.forEach((cats) => {
    const closed = data.totalGrievancesClosedByCategory
      ? data.totalGrievancesClosedByCategory[cats.toString()]
      : 0;
    const open = data.totalGrievancesOpenedByCategory
      ? data.totalGrievancesOpenedByCategory[cats.toString()]
      : 0;
      ctContainer.table.body.push([
      `${cats.toString()}`,
      { text: `${open || 0}`, alignment: "right" },
      { text: `${closed || 0}`, alignment: "right" },
    ]);
  });
  content.push(ctContainer);

  content.push({ text: "Total Grievances by Status", style: "subheader" });
  const stContainer: any = {
    table: {
      widths: ["*", "auto"],
      style: "tableExample",
      body: [
        [
          { text: "Status Name", style: "tableHeader" },
          { text: "Count", style: "tableHeader" },
        ],
      ],
    },
  };
  Object.keys(data.totalGrievancesByStatus || {}).forEach((st: string) => {
    const key = st as Statuses;
    const grs = data.totalGrievancesByStatus;
    stContainer.table.body.push([
      `${st.toLocaleUpperCase()}`,
      { text: grs ? `${grs[key]}` : 0 },
    ]);
  });
  content.push(stContainer);
  content.push({
    text: "This is a computer generated report of grievances from www.pixiegrs.com. ",
    style: "footerText",
  });

  definition.content = content;
  return definition;
}

export function generateGreivanceReportDefinitionForPDF(
  grievance: GrievanceType,
  responses: GResponseType[],
  users: UserType[]
) {
  const definition: any = {};
  definition.styles = styles;
  let content = [];
  content.push({ text: "GRS Report", style: "header" });
  const grsAuthor = users.find((u) => u.id === grievance.userId);
  content.push({
    columns: [
      {
        text: [
          { text: `${grsAuthor?.name}`, fontSize: 13, bold: true },
          "\n",
          `${grsAuthor?.email}`,
          "\n",
          `${grsAuthor?.department}`,
        ],
        width: "30%",
        style: "fontXs",
      },
      {
        text: [
          {
            text: `Created on ${_d(grievance.createdAt).format(
              "DD-MM-YYYY hh:mm:ss A"
            )}`,
          },
          "\n",
          {
            text: `Updated on ${_d(grievance.updatedAt).format(
              "DD-MM-YYYY hh:mm:ss A"
            )}`,
          },
          "\n",
          `ID: ${grievance.key}`,
          "\n",
          "",
        ],
        width: "30%",
        style: "fontXs",
      },
      {
        text: [
          {
            text: "Status:",
            fontSize: 14,
          },
          {
            text: `${grievance.status}`,
            style: "bold",
            fontSize: 14,
          },
          "\n",
          `${grievance.priority.replace(/_/g, " ")}`,
          "\n",
          {
            text: `${grievance.category.replace(/_/g, " ")}`,
            fontSize: 11,
            style: "boldItalic",
          },
        ],
        style: "headerRight",
      },
    ],
  });
  content.push({
    table: {
      widths: ["*"],
      margin: [0, 15, 0, 15],
      style: "tableExample",
      body: [[`${grievance.content}`]],
    },
  });
  content.push({
    text: "Responses",
    style: "subheader",
  });
  const rContainer: any = {
    table: {
      widths: ["*", "auto"],
      style: "tableExample",
      body: [],
    },
  };
  responses.forEach((r) => {
    const user = users.find((u) => u.id === r.userId);
    rContainer.table.body.push([
      `${r.content}`,
      { text: `${user?.name}`, alignment: "right" },
    ]);
  });
  content.push(rContainer);
  definition.content = content;
  return definition;
}
