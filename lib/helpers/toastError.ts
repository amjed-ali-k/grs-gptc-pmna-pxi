import toast from "react-hot-toast";

export function toastError(data: any) {
  if (data && data.error) toast.error(data.error);
  if (data && data.data && data.data.error) toast.error(data.data.error);
}
