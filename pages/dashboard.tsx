import type { NextPage } from "next";
import {
  MdClearAll,
  MdOutlineDoneAll,
  MdOutlineHourglassBottom,
  MdOutlineDeleteForever,
} from "react-icons/md";

import RoundIcon from "../components/RoundIcon";
import PageTitle from "../components/Typography/PageTitle";
import CTA from "../components/CTA";
import InfoCard from "../components/Cards/InfoCard";
import Layout from "../containers/Layout";
import MyGrievances from "../components/Tables/MyGrievances";
import Title from "../components/Ui/Title";
import { useFetchData } from "../lib/adapters/fetcher";
import { UserGrievanceStatusCount } from "../lib/types/response";

const Home: NextPage = () => {
  return (
    <Layout>
      <PageTitle>Dashboard</PageTitle>
 

      {/* <!-- Cards --> */}
      <InfoCards />
      <Title>My Grievances</Title>
      <MyGrievances />
    </Layout>
  );
};

export default Home;

function InfoCards() {
  const { data } = useFetchData<UserGrievanceStatusCount>(
    "/api/grievances/status/me"
  );
  return (
    <div className="grid gap-6 mb-8 md:grid-cols-2 xl:grid-cols-4">
      <InfoCard title="Total Grievances" value={data?.total || 0}>
        <RoundIcon
          icon={MdClearAll}
          iconColorClass="text-orange-500 dark:text-orange-100"
          bgColorClass="bg-orange-100 dark:bg-orange-500"
          className="mr-4"
        />
      </InfoCard>

      <InfoCard title="Total Solved" value={data?.solved || 0}>
        <RoundIcon
          icon={MdOutlineDoneAll}
          iconColorClass="text-green-500 dark:text-green-100"
          bgColorClass="bg-green-100 dark:bg-green-500"
          className="mr-4"
        />
      </InfoCard>

      <InfoCard title="Proccesing Grievances" value={data?.processing || 0}>
        <RoundIcon
          icon={MdOutlineHourglassBottom}
          iconColorClass="text-blue-500 dark:text-blue-100"
          bgColorClass="bg-blue-100 dark:bg-blue-500"
          className="mr-4"
        />
      </InfoCard>

      <InfoCard title="Rejected Grievances" value={data?.rejected || 0}>
        <RoundIcon
          icon={MdOutlineDeleteForever}
          iconColorClass="text-red-500 dark:text-red-100"
          bgColorClass="bg-red-100 dark:bg-red-500"
          className="mr-4"
        />
      </InfoCard>
    </div>
  );
}
