import React from "react";
import { Form, Formik } from "formik";
import * as Yup from "yup";
import Link from "next/link";
import Image from "next/image";
import { useRouter } from "next/router";
import toast, { Toaster } from "react-hot-toast";
import { signIn } from "next-auth/react";
import {
  BsFacebook,
  BsGithub,
  BsGoogle,
  BsLinkedin,
  BsTwitter,
} from "react-icons/bs";
import { Button } from "@windmill/react-ui";

import TextInput from "../components/Forms/TextInput";
import FormButton from "../components/Forms/FormButton";
import { useFetchPermenantData } from "../lib/adapters/fetcher";

function Login() {
  const router = useRouter();
  const { data: providers } = useFetchPermenantData<{ [key: string]: any }>(
    "/api/auth/providers"
  );
  return (
    <div className="flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900">
      <Toaster />
      <div className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800">
        <div className="flex flex-col overflow-y-auto md:flex-row">
          <div className="relative h-32 md:h-auto md:w-1/2">
            <Image
              src="/images/login.jpg"
              alt="New account"
              layout="fill"
              objectFit="cover"
            />
          </div>
          <main className="flex items-center justify-center p-6 sm:p-12 md:w-1/2">
            <div className="w-full">
              <h1 className="mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200">
                Login
              </h1>
              <Formik
                initialValues={{
                  email: "",
                  password: "",
                }}
                validationSchema={Yup.object({
                  email: Yup.string()
                    .email("Invalid email address")
                    .required("Required"),
                  password: Yup.string()
                    .min(5, "Must be 5 characters or less")
                    .required("Required"),
                })}
                onSubmit={async (values, { setSubmitting }) => {
                  const { ok } = (await signIn("credentials", {
                    redirect: false,
                    ...values,
                  })) as unknown as { ok: boolean };
                  if (ok) {
                    toast.success("Authentication Successful");
                    router.push("/dashboard");
                  } else {
                    toast.error("Invalid Credentials");
                  }
                  setSubmitting(false);
                }}
              >
                <Form>
                  <TextInput
                    label="Email"
                    name="email"
                    type="text"
                    placeholder="Enter your email"
                  />
                  <TextInput
                    label="Password"
                    name="password"
                    type="password"
                    placeholder="***************"
                  />
                  <FormButton> Login </FormButton>
                </Form>
              </Formik>
              <hr className="my-8" />
              <p className="my-4 font-bold text-center text-white">
                Or Login with
              </p>
              {providers?.github && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("github", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsGithub}
                >
                  Github
                </Button>
              )}
              {providers?.google && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("google", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsGoogle}
                >
                  Google
                </Button>
              )}
              {providers?.linkedin && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("linkedin", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsLinkedin}
                >
                  LinkedIn
                </Button>
              )}
              {providers?.facebook && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("facebook", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsFacebook}
                >
                  Facebook
                </Button>
              )}
              {providers?.twitter && (
                <Button
                  className="m-1"
                  onClick={() => {
                    signIn("twitter", { callbackUrl: "/dashboard" });
                  }}
                  icon={BsTwitter}
                >
                  Twitter
                </Button>
              )}
              <p className="mt-4">
                <Link href="/forgot-password">
                  <a className="text-sm font-medium text-purple-600 dark:text-purple-400 hover:underline">
                    Forgot your password?
                  </a>
                </Link>
              </p>
              <p className="mt-1">
                <Link href="/create-account">
                  <a className="text-sm font-medium text-purple-600 dark:text-purple-400 hover:underline">
                    Create account
                  </a>
                </Link>
              </p>
            
            </div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default Login;
