import { GrTypeType } from "../../../lib/constants/grievance";
import { generateAndSaveMonthlyReport } from "../../../lib/db/reports";
import backendGet from "../../../lib/helpers/backendGet";

export default backendGet(async (req) => {
    return generateAndSaveMonthlyReport(1, 2022, GrTypeType.STAFF);
})