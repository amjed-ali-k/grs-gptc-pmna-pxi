import { GrTypeType } from "../../../lib/constants/grievance";
import { Roles } from "../../../lib/constants/users";
import { generateAndSaveMonthlyReport } from "../../../lib/db/reports";
import backendGet from "../../../lib/helpers/backendGet";
import { getEmail, validateRole } from "../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
    const email = await getEmail(req);
    if (!email) throw new Error("You are not allowed for this operation");
    const isAdmin = await validateRole([Roles.ADMIN], req, email);
    if(!isAdmin) throw new Error("Unauthorized")
    const { month, year, type } = req.query;
    const val = await generateAndSaveMonthlyReport(parseInt(month.toString()), parseInt(year.toString()), type.toString() as GrTypeType);
    if(val === false) throw new Error("An unknown error occured")
    return val
})