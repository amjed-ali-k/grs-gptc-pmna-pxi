import { SettingsType } from "../../../lib/constants/settings";
import { getSettings } from "../../../lib/db/settings";
import backendGet from "../../../lib/helpers/backendGet";

export default backendGet(async () => {
  return getSettings(SettingsType.FrontPage);
});
