import { SettingsType } from "../../../../lib/constants/settings";
import { Permissions } from "../../../../lib/constants/users";
import { getSettings } from "../../../../lib/db/settings";
import backendGet from "../../../../lib/helpers/backendGet";
import {
  getEmail,
  validatePermissions,
} from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("Unauthorized");
  if (await validatePermissions(email, [Permissions.CREATE_INTERNAL_COMPLAINTS]))
    return getSettings(SettingsType.InternalGrievance);
  throw new Error("Unauthorized");
});
