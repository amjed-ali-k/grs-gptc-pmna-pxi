import { AuthSettings, SettingsType } from "../../../lib/constants/settings";
import { getSettings } from "../../../lib/db/settings";
import backendGet from "../../../lib/helpers/backendGet";

export default backendGet(async (req) => {
  const settings = await getSettings<AuthSettings>(SettingsType.Auth);
  if (!settings) throw new Error("Unknown");
  if (settings.key !== SettingsType.Auth) throw new Error("Unknown");
  return {
    enableRecaptcha: settings.enableRecaptcha,
    recaptchaSiteKey: settings.recaptchaSiteKey,
    passwordMinLength: settings.passwordMinLength,
    passwordMaxLength: settings.passwordMaxLength,
    passwordUppercaseLetter: settings.passwordUppercaseLetter,
    passwordLowercaseLetter: settings.passwordLowercaseLetter,
    passwordNumber: settings.passwordNumber,
    passwordSpecialCharacter: settings.passwordSpecialCharacter,
    semesters : settings.semesters,
    designations: settings.designations,
    departments: settings.departments,
  };
});
