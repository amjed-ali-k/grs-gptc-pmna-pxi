// Generate Database backup
import { Deta } from "deta";

import { getProfileByEmail } from "../../../../lib/db/profile";
import { getRole } from "../../../../lib/db/roles";
import backendGet from "../../../../lib/helpers/backendGet";
import { getEmail } from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
    const email = await getEmail(req);
    if (!email) throw new Error("Not Authenticated");
    const user = await getProfileByEmail(email);
    if (!user) throw new Error("No user found");
    const roles = await getRole(user.key);
    if (!roles) throw new Error("No roles found");
    if (!roles.roles.includes("admin")) throw new Error("Not Authorized");
    
  const dbs = [
    "auth_users",
    "auth_user_accounts",
    "auth_user_sessions",
    "auth_user_token",
    "user_profile",
    "user_roles",
    "settings_role_permissions",
    "grievances",
    "responses",
    "system_settings",
  ];
  const deta = Deta(process.env.DETA_PROJECT_KEY);
  let database: {[key:string]: any} = {}
  await Promise.all(
    dbs.map(async (db) => {
      let res = await deta.Base(db).fetch({});
      let allItems = res.items;

      // continue fetching until last is not seen
      while (res.last) {
        res = await deta.Base(db).fetch({}, { last: res.last });
        allItems = allItems.concat(res.items);
      }
      database[db] = allItems
      console.log(allItems)
      return db;
    })
  );
  return database
});
