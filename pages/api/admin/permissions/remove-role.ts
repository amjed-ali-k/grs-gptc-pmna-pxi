import * as Yup from "yup";

import validatedPost from "../../../../lib/helpers/validatedPost";
import { getEmail, validateRole } from "../../../../lib/helpers/validateRoles";
import { Roles } from "../../../../lib/constants/users";
import { removePermission } from "../../../../lib/db/roles";

const userValidationSchema: Yup.SchemaOf<{}> = Yup.object()
  .shape({ permission: Yup.string().required(), role: Yup.string().required() })
  .noUnknown();

export default validatedPost(userValidationSchema, async (data, req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("You are not allowed for this operation");

  const isAdmin = await validateRole([Roles.ADMIN], req, email);
  if (!isAdmin) throw new Error("You are not allowed for this operation");
  return removePermission(data.role, data.permission);
});
