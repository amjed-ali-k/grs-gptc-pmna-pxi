import { SettingsType } from "../../../../lib/constants/settings";
import { Roles } from "../../../../lib/constants/users";
import { getSettings } from "../../../../lib/db/settings";
import backendGet from "../../../../lib/helpers/backendGet";
import { getEmail, validateRole } from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("Unauthorized");
  if (!(await validateRole([Roles.ADMIN], req, email)))
    throw new Error("Unauthorized");
  const { type } = req.query;
  return getSettings<any>(type.toString() as SettingsType);
});
