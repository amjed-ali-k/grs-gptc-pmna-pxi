import * as Yup from "yup";

import validatedPost from "../../../../lib/helpers/validatedPost";
import { getEmail, validateRole } from "../../../../lib/helpers/validateRoles";
import { Roles } from "../../../../lib/constants/users";
import { SettingsType } from "../../../../lib/constants/settings";
import { updateSettings } from "../../../../lib/db/settings";

const settingsValidationSchema: Yup.SchemaOf<{}> = Yup.object()
  .shape({ type: Yup.string().oneOf(Object.values(SettingsType)).required(), settings: Yup.object().required() })
  .noUnknown();

export default validatedPost(settingsValidationSchema, async (data, req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("You are not allowed for this operation");

  const isAdmin = await validateRole([Roles.ADMIN], req, email);
  if (!isAdmin) throw new Error("You are not allowed for this operation");
  console.log(data.settings)
  return updateSettings(data.type, data.settings);
});
