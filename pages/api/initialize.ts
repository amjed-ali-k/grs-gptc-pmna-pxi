import { updatePermissions } from "../../lib/db/roles";
import backendGet from "../../lib/helpers/backendGet";

export default backendGet(async (req) => {
  await updatePermissions("admin", [
    "create-staff-grievance",
    "view-staff-grievance",
    "update-staff-grievance",
    "create-student-grievance",
    "view-student-grievance",
    "update-student-grievance",
    "create-internal-complaints",
    "view-internal-complaints",
    "update-internal-complaints",
    "delete-grievance",
    "create-staff-grievance-response",
    "delete-staff-grievance-response",
    "create-student-grievance-response",
    "delete-student-grievance-response",
    "create-internal-complaints-response",
    "delete-internal-complaints-response",
    "view-staff-user",
    "view-student-user",
    "update-user",
    "delete-user",
  ]);
  await updatePermissions("student-greivence-cell-member", [
    "view-student-grievance",
    "update-student-grievance",
    "create-student-grievance-response",
    "delete-student-grievance-response",
  ]);
  await updatePermissions("staff-greivence-cell-member", [
    "view-staff-grievance",
    "update-staff-grievance",
    "create-staff-grievance-response",
    "delete-staff-grievance-response",
  ]);
  await updatePermissions("internal-complaints-cell-member", [
    "view-internal-complaints",
    "update-internal-complaints",
    "create-internal-complaints-response",
    "delete-internal-complaints-response",
  ]);
  await updatePermissions("staff", ["create-staff-grievance"]);
  await updatePermissions("parent", ["create-student-grievance"]);
  await updatePermissions("student", ["create-student-grievance"]);
  await updatePermissions("guest", []);
});
