import * as yup from "yup";

import validatedPost from "../../../lib/helpers/validatedPost";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";
import {
  deleteResponse,
  getGrievance,
  getResponse,
  updateGrievance,
} from "../../../lib/db/grievances";
import { getProfileByEmail } from "../../../lib/db/profile";
import { Permissions } from "../../../lib/constants/users";
import { GrTypeType } from "../../../lib/constants/grievance";

const userValidationSchema: yup.SchemaOf<{}> = yup
  .object()
  .shape({
    grievanceId: yup.string().required(),
    responseId: yup.string().required(),
  })
  .noUnknown();

export default validatedPost(userValidationSchema, async (data, req) => {
  const email = await getEmail(req);
  if (!email) return;
  const user = await getProfileByEmail(email);
  if (!user) return;
  const _data = { ...data } as { grievanceId: string; responseId: string };

  const gr = await getGrievance(_data.grievanceId);
  if (!gr) return;
  const response = await getResponse(_data.responseId);
  if (!response) return;
  if (
    user.id === response.userId ||
    (gr.type === GrTypeType.STAFF &&
      (await validatePermissions(email, [
        Permissions.DELETE_STAFF_GRIEVANCE_RESPONSE,
      ]))) ||
    (gr.type === GrTypeType.STUDENT &&
      (await validatePermissions(email, [
        Permissions.DELETE_STUDENT_GRIEVANCE_RESPONSE,
      ]))) ||
    (gr.type === GrTypeType.INTERNAL &&
      (await validatePermissions(email, [
        Permissions.DELETE_INTERNAL_COMPLAINTS_RESPONSE,
      ])))
  ) {
    await deleteResponse(data.responseId);
    return updateGrievance(
      {
        totalResponses: gr.totalResponses - 1,
        notifications:
          (gr.unReadResponseIds
            ? gr.unReadResponseIds.length
            : gr.notifications) - 1,
        unReadResponseIds: [...gr.unReadResponseIds.filter(
          (id) => id !== data.responseId
        )],
      },
      _data.grievanceId
    );
  }
  throw new Error("You do not have permission to create a response");
});
