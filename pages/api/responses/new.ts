import * as yup from "yup";
import { getClientIp } from "request-ip";

import validatedPost from "../../../lib/helpers/validatedPost";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";
import { responseSchema } from "../../../lib/schemas/grievances";
import {
  createNewResponse,
  getGrievance,
  updateGrievance,
} from "../../../lib/db/grievances";
import { NewResponseApi, ResponseInput } from "../../../lib/types/db";
import { getProfileByEmail } from "../../../lib/db/profile";
import { APIError, Permissions } from "../../../lib/constants/users";
import { GrTypeType } from "../../../lib/constants/grievance";
import { getSettings } from "../../../lib/db/settings";
import { GrievanceSettings } from "../../../lib/constants/settings";
import { newResponseAction } from "../../../lib/actions/responses";

const userValidationSchema: yup.SchemaOf<NewResponseApi> = yup
  .object()
  .shape(responseSchema)
  .noUnknown();

export default validatedPost(userValidationSchema, async (data, req) => {
  const email = await getEmail(req);
  if (!email) return;
  const user = await getProfileByEmail(email);
  if (!user) return;
  const _data = { ...data } as ResponseInput;
  const gr = await getGrievance(_data.grievanceId);
  if (!gr) return;
  const settings = await getSettings<GrievanceSettings>(gr.type as any);
  if (
    (gr.type === GrTypeType.STAFF &&
      (await validatePermissions(email, [
        Permissions.CREATE_STAFF_GRIEVANCE_RESPONSE,
      ]))) ||
    (gr.type === GrTypeType.STUDENT &&
      (await validatePermissions(email, [
        Permissions.CREATE_STUDENT_GRIEVANCE_RESPONSE,
      ]))) ||
    (gr.type === GrTypeType.INTERNAL &&
      (await validatePermissions(email, [
        Permissions.CREATE_INTERNAL_COMPLAINTS_RESPONSE,
      ]))) ||
    (gr.userId === user.id && settings.allowAuthorResponse)
  ) {
    const res = await createNewResponse({
      ..._data,
      userId: user.key,
      userEmail: user.email,
      ip: getClientIp(req),
      prevStatus: gr.status,
    });
    if (!res) return APIError.NO_RESPONSE;
    const notifications =
      user.key === gr.userId ? gr.notifications : gr.notifications + 1;
    gr.unReadResponseIds.push(res.key);
    const ret = updateGrievance(
      {
        status: _data.status,
        totalResponses: gr.totalResponses + 1,
        notifications,
        unReadResponseIds: user.key === gr.userId ? [] : gr.unReadResponseIds,
      },
      _data.grievanceId
    );
    await newResponseAction(res, gr, user)
    return ret;
  }
  throw new Error("You do not have permission to create a response");
});
