import * as yup from "yup";

import validatedPost from "../../../lib/helpers/validatedPost";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";
import { getProfileByEmail } from "../../../lib/db/profile";
import { deleteGrievance, getGrievance } from "../../../lib/db/grievances";
import { Permissions } from "../../../lib/constants/users";
import { GrTypeType } from "../../../lib/constants/grievance";

const userValidationSchema: yup.SchemaOf<{}> = yup
  .object()
  .shape({
    id: yup.string().required(),
  })
  .noUnknown();

export default validatedPost(userValidationSchema, async (data, req) => {
  const email = await getEmail(req);
  if (!email) return;
  const user = await getProfileByEmail(email);
  if (!user) return;
  const { id } = data as { id: string };
  const gr = await getGrievance(id);
  if (!gr) return;
  if (
    gr.userEmail === user.email ||
    (await validatePermissions(email, [Permissions.DELETE_ALL_GRIEVANCE])) ||
    (gr.type === GrTypeType.STAFF &&
      Permissions.DELETE_STAFF_GRIEVANCE_RESPONSE) ||
    (gr.type === GrTypeType.STUDENT &&
      Permissions.DELETE_STUDENT_GRIEVANCE_RESPONSE) ||
    (gr.type === GrTypeType.INTERNAL &&
      Permissions.DELETE_INTERNAL_COMPLAINTS_RESPONSE)
  ) {
    return deleteGrievance(id);
  }
  throw new Error("Unauthorized");
});
