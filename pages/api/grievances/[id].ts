import { GrTypeType } from "../../../lib/constants/grievance";
import { Permissions } from "../../../lib/constants/users";
import { getGrievance, updateGrievance } from "../../../lib/db/grievances";
import backendGet from "../../../lib/helpers/backendGet";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";


export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) return;
  const { id } = req.query;
  const gr = await getGrievance(id.toString());
  if (!gr) return;
  if (gr.userEmail === email) {
    await updateGrievance({ notifications: 0, unReadResponseIds: [] }, id.toString());
  }

  let canView = async () => {
    if (email === gr.userEmail) return true;
    if (gr.type === GrTypeType.STAFF)
      return validatePermissions(email, [Permissions.VIEW_STAFF_GRIEVANCE]);
    if (gr.type === GrTypeType.STUDENT)
      return validatePermissions(email, [Permissions.VIEW_STUDENT_GRIEVANCE]);
    if (gr.type === GrTypeType.INTERNAL)
      return validatePermissions(email, [Permissions.VIEW_INTERNAL_COMPLAINTS]);
    return validatePermissions(email, [Permissions.VIEW_ALL_GRIEVANCES]);
  };
  if (await canView()) return gr;

  return null;
});
