import { getUserByEmail } from "../../../../lib/db/auth";
import { getGrievanceCountByStatusByUser } from "../../../../lib/db/status";
import backendGet from "../../../../lib/helpers/backendGet";
import { getEmail } from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("Not Authenticated");
  const userObj = await getUserByEmail(email);
  if (!userObj) throw new Error("User not found");
  return getGrievanceCountByStatusByUser(userObj);
});
