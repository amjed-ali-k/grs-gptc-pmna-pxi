import { NextApiRequest } from "next";
import { ObjectShape } from "yup/lib/object";
import * as yup from "yup";
import { getClientIp } from "request-ip";

import validatedPost from "../../../lib/helpers/validatedPost";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";
import {
  getGrievanceSchema,
  grievanceSchema,
} from "../../../lib/schemas/grievances";
import { createNewGrievance } from "../../../lib/db/grievances";
import { GrievanceInput } from "../../../lib/types/db";
import { getProfileByEmail } from "../../../lib/db/profile";
import { Permissions } from "../../../lib/constants/users";
import { GrTypeType } from "../../../lib/constants/grievance";
import { getSettings } from "../../../lib/db/settings";
import { SettingsType } from "../../../lib/constants/settings";
import { LogType, newLog } from "../../../lib/db/logs";

function schemaWrapper(shape: ObjectShape) {
  return yup.object().shape(shape).noUnknown();
}

async function validation(req: NextApiRequest) {
  const data = req.body as {
    type:
      | SettingsType.StudentGrievance
      | SettingsType.StaffGrievance
      | SettingsType.InternalGrievance;
  };
  const settings = await getSettings(data.type);
  if (!settings) return schemaWrapper(grievanceSchema);
  return schemaWrapper(getGrievanceSchema(settings as any)); // Type escape
}

export default validatedPost(validation, async (data: GrievanceInput, req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("Not Authenticated");
  const user = await getProfileByEmail(email);
  if (!user) throw new Error("No user found");
  if (
    (data.type === GrTypeType.STAFF &&
      (await validatePermissions(email, [
        Permissions.CREATE_STAFF_GRIEVANCE,
      ]))) ||
    (data.type === GrTypeType.STUDENT &&
      (await validatePermissions(email, [
        Permissions.CREATE_STUDENT_GRIEVANCE,
      ]))) ||
    (data.type === GrTypeType.INTERNAL &&
      (await validatePermissions(email, [
        Permissions.CREATE_INTERNAL_COMPLAINTS,
      ])))
  ) {
    const newgrs = await createNewGrievance({
      ...data,
      userId: user.key,
      userEmail: user.email,
      ip: getClientIp(req),
    });
    if (!newgrs) throw new Error("Error creating grievance");
    await newLog(
      LogType.NEW_GRIEVANCE,
      {
        ip: getClientIp(req),
        type: data.type,
        id: newgrs.key,
        category: data.category,
        department: user.department,
        priority: data.priority,
      },
      user.key,
      data.type
    );
    return newgrs;
  }
  throw new Error("Unauthorized");
});
