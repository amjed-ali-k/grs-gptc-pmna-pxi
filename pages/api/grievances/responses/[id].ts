import { GrTypeType } from "../../../../lib/constants/grievance";
import { Permissions } from "../../../../lib/constants/users";
import {
  getGrievance,
  getResponsesForGreivance,
} from "../../../../lib/db/grievances";
import backendGet from "../../../../lib/helpers/backendGet";
import {
  getEmail,
  validatePermissions,
} from "../../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) return;
  const { id } = req.query;
  const gr = await getGrievance(id.toString());
  if (!gr) return;
  const rs = await getResponsesForGreivance(id.toString());
  if (!rs) return;
  if (
    email === gr.userEmail ||
    (gr.type === GrTypeType.STAFF &&
      (await validatePermissions(email, [Permissions.VIEW_STAFF_GRIEVANCE]))) ||
    (gr.type === GrTypeType.STUDENT &&
      (await validatePermissions(email, [
        Permissions.VIEW_STUDENT_GRIEVANCE,
      ]))) ||
    (gr.type === GrTypeType.INTERNAL &&
      (await validatePermissions(email, [
        Permissions.VIEW_INTERNAL_COMPLAINTS,
      ]))) ||
    (await validatePermissions(email, [Permissions.VIEW_ALL_GRIEVANCES]))
  ) {
    return rs;
  }
  throw new Error("Not authorized");
});
