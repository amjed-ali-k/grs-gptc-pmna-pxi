import * as yup from "yup";
import bcrypt from "bcryptjs";
import validatedPost from "../../../lib/helpers/validatedPost";
import { createNewUser, getProfileByEmail } from "../../../lib/db/profile";
import { getUserByEmail } from "../../../lib/db/auth";
import { getSettings } from "../../../lib/db/settings";
import { AuthSettings, SettingsType } from "../../../lib/constants/settings";
import { ObjectShape } from "yup/lib/object";
import { NextApiRequest } from "next";
import { LogType, newLog } from "../../../lib/db/logs";
import { sendRegistrationEmail } from "../../../lib/notifications/email";

const defaultObjectShape = {
  name: yup.string().min(5).required(),
  email: yup.string().email().required(),
  password: yup.string().required(),
  active: yup.bool().default(() => true),
  registered: yup.number().default(function () {
    return Date.now();
  }),
  updatedAt: yup.number().default(function () {
    return Date.now();
  }),
};

function schemaWrapper(shape: ObjectShape) {
  return yup.object().shape(shape).noUnknown();
}

async function schema(req: NextApiRequest) {
  const settings = await getSettings<AuthSettings>(SettingsType.Auth);
  if (!settings) return schemaWrapper(defaultObjectShape);

  return schemaWrapper({
    ...defaultObjectShape,
    password: yup
      .string()
      .min(settings.passwordMinLength)
      .max(settings.passwordMaxLength)
      .test("isValidPass", " is not valid", (value, context) => {
        if (!value) return false;
        const hasUpperCase = /[A-Z]/.test(value);
        const hasLowerCase = /[a-z]/.test(value);
        const hasNumber = /[\d]/.test(value);
        const hasSymbol = /[!@#%&]/.test(value);
        if (settings.passwordUppercaseLetter && !hasUpperCase) return false;
        if (settings.passwordLowercaseLetter && !hasLowerCase) return false;
        if (settings.passwordNumber && !hasNumber) return false;
        if (settings.passwordSpecialCharacter && !hasSymbol) return false;
        return true;
      })
      .required(),
  });
}

export default validatedPost(schema, async (data) => {
  const user = { ...data };
  const settings = await getSettings<AuthSettings>(SettingsType.Auth);
  if (!settings.allowRegistration) throw new Error("Registration is disabled");
  const domain = user.email.substring(user.email.lastIndexOf("@") + 1);
  if (settings.blacklistedDomains.includes(domain))
    throw new Error(`Domain ${domain} is blacklisted`);
  if (
    settings.whiteListedDomains.length > 0 &&
    !settings.whiteListedDomains.includes(domain)
  )
    throw new Error(`Domain ${domain} is not whitelisted`);
  user.password = await bcrypt.hash(user.password, 10);
  const _user = await getProfileByEmail(user.email);
  const _euser = await getUserByEmail(user.email);
  if (_user || _euser) throw new Error("User already exists");
  const us = await createNewUser(user);
  await newLog(LogType.NEW_USER, us, us?.key as string,);
  await sendRegistrationEmail(user)
});
