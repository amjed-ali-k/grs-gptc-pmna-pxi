import { APIError } from "../../../lib/constants/users";
import { getUserByEmail } from "../../../lib/db/auth";
import { getProfileById } from "../../../lib/db/profile";
import { getRoleByEmailWithPermissions } from "../../../lib/db/roles";
import backendGet from "../../../lib/helpers/backendGet";
import { getEmail } from "../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error(APIError.UNAUTHORIZED);
  const gr = await getUserByEmail(email);
  if (!gr) throw new Error(APIError.UNAUTHORIZED);
  const profile = await getProfileById(gr.id);
  if (!profile) throw new Error(APIError.NO_PROFILE);
  if (email !== gr.email) throw new Error(APIError.NO_PROFILE);
  const permissions = await getRoleByEmailWithPermissions(email);
  return {
    ...gr,
    ...profile,
    ...permissions,
    key: gr.id,
    password: undefined,
    hasPassword: profile.password? true : false,
  };
});
