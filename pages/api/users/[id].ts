import { Permissions } from "../../../lib/constants/users";
import { getUser } from "../../../lib/db/auth";
import { getProfileById } from "../../../lib/db/profile";
import backendGet from "../../../lib/helpers/backendGet";
import {
  getEmail,
  validatePermissions,
} from "../../../lib/helpers/validateRoles";

export default backendGet(async (req) => {
  const email = await getEmail(req);
  if (!email) throw new Error("Not authorized");
  const { id } = req.query;
  const gr = await getUser(id.toString());
  if (!gr) throw new Error("Not authorized");
  const profile = await getProfileById(gr.id);
  if (!profile) throw new Error("Not completed profile");
  if (
    email === gr.email ||
    ((await validatePermissions(email, [Permissions.VIEW_STAFF_USER])) &&
      profile.type === "staff") ||
    ((await validatePermissions(email, [Permissions.VIEW_STUDENT_USER])) &&
      profile.type === "student") ||
    ((await validatePermissions(email, [Permissions.VIEW_PARENT_USER])) &&
      profile.type === "parent")
  ) {
    return {
      ...gr,
      ...profile,
      password: undefined,
    };
  }
  let des: string | undefined;
  if (profile.type === "staff" && profile.designation) {
    des = profile.designation;
  } else if (profile.department) {
    des = profile.department;
  } else {
    des = "";
  }
  return { ...gr, designation: des, department: profile.department, email: undefined };
});
