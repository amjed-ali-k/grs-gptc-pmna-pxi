/* 
    This is the reports page.
    1. Monthly reports
    2. Weekly reports
    3. Daily reports
    4. User reports
    5. Access logs
    6. PDF Generation
    7. Excel Generation
    8. CSV Generation
    9. PDF For NBA Accreditation Generation on individual pages.
*/
import pdfMake from "pdfmake/build/pdfmake";
import toast from "react-hot-toast";
import pdfFonts from "pdfmake/build/vfs_fonts";
pdfMake.vfs = pdfFonts.pdfMake.vfs;

import { FormikCard, Masonary } from "../../components/Forms/FormCard";
import FormDatePicker from "../../components/Forms/FormDatePicker";
import SelectInput from "../../components/Forms/SelectInput";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import { generateGreivanceReportDefinitionForPDF, generateReportPdf } from "../../lib/helpers/generateReportPdf";
import httpClient from "../../lib/helpers/httpClient";
import { toastError } from "../../lib/helpers/toastError";
import { GrieavancesWithResponses, UserType } from "../../lib/types/db";

function CustomPage() {
  return (
    <Layout>
      <PageTitle>Reports</PageTitle>
      <Masonary large>
        <FormikCard
          title="Generate Reports"
          description="Generate reports of grievances in diffrent formats."
          onSubmit={async (values) => {
            console.log(values);
            const { data } = await httpClient.get(
              `/api/reports/monthlypdf?month=${values.month.getMonth()}&year=${values.month.getFullYear()}&type=${
                values.type
              }`
            );
            if (data) {
              const def = generateReportPdf(data);
              try {
                pdfMake.createPdf(def).download();
              } catch (error) {
                console.log(error);
              }
            } else toastError("An error occured");
            toast.success("Generated Report");
          }}
          initialValues={{
            type: "",
            month: "",
          }}
        >
          <SelectInput
            label="Select Report"
            name="duration"
            defaultValue={"monthly"}
          >
            <option>Select</option>
            <option value="monthly">Monthly</option>
            <option value="weekly" disabled>
              Weekly
            </option>
            <option value="daily" disabled>
              Daily
            </option>
            <option value="custom" disabled>
              Custom
            </option>
          </SelectInput>
          <SelectInput label="Select Grievance Type" name="type">
            <option>Select</option>
            <option value="student">Student Grievances</option>
            <option value="staff">Staff Grievances </option>
            <option value="internal">Internal Complaints</option>
          </SelectInput>

          <div className="px-1">
            <FormDatePicker
              name="month"
              label="Select Month"
              dateFormat="MM/yyyy"
              showMonthYearPicker
              showFullMonthYearPicker
            />
          </div>

          {/* <SelectInput label="Select Status Type" name="status-type">
            <option value="all">All</option>
            <option value="open">Open</option>
            <option value="closed">Closed</option>
            <option value="proccessing">Processing</option>
            <option value="rejected">Rejected</option>
          </SelectInput> */}
          <SelectInput
            label="Select Report Format"
            name="format"
            defaultValue={"pdf"}
          >
            <option value="pdf">PDF</option>
            <option value="excel" disabled>
              Excel
            </option>
            <option value="csv" disabled>
              CSV
            </option>
          </SelectInput>
        </FormikCard>
        <FormikCard
          title="Individual Grievance (PDF)"
          description="Generate individual grievance reports in PDF format which can be used in documatation for accreditations like NBA."
          onSubmit={async (values) => {
            const { data } = await httpClient.get<{
              grievances: GrieavancesWithResponses[];
              users: UserType[];
            }>(
              `/api/reports/singlepdf?fromDate=${values.fromDate.getTime()}&toDate=${values.toDate.getTime()}&type=${
                values.grievancetype
              }`
            );
            for(const val of data.grievances){
              let definition = generateGreivanceReportDefinitionForPDF(val, val.responses, data.users);
              

            }
          }}
          initialValues={{
            grievancetype: "",
            fromDate: "",
            toDate: "",
          }}
        >
          <SelectInput label="Select Grievance Type" name="grievancetype">
            <option>Select...</option>
            <option value="student">Student Grievances</option>
            <option value="staff">Staff Grievances </option>
            <option value="internal">Internal Complaints</option>
          </SelectInput>
          <div className="flex flex-wrap lg:flex-nowrap">
            <div className="px-1">
              <FormDatePicker name="fromDate" label="Grievances opened since" />
            </div>
            <div className="px-1">
              <FormDatePicker name="toDate" label="To Date" />
            </div>
          </div>
        </FormikCard>
      </Masonary>
    </Layout>
  );
}

export default CustomPage;

/*

*/
