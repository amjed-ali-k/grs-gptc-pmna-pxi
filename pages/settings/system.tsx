import { Button } from "@windmill/react-ui";
import toast from "react-hot-toast";
import { useSWRConfig } from "swr";
import CheckBox from "../../components/Forms/CheckBox";
import {
  FormCard,
  FormikCard,
  Masonary,
} from "../../components/Forms/FormCard";
import TextAreaInput from "../../components/Forms/TextAreaInput";
import TextInput from "../../components/Forms/TextInput";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import {
  FrontPageSettings,
  InstitutionSettings,
  IntegrationSettings,
  SettingsType,
} from "../../lib/constants/settings";
import httpClient from "../../lib/helpers/httpClient";
import { toastError } from "../../lib/helpers/toastError";

/* 
    This is the settings page.
    1. College Name and Details
    2. System Settings
    3. Integrations - Whatsapp & Telegram
    4. Frontpage Settings
    5. Backup and Restore
    6. Add Rules Text for New Grievances
    7. Add Terms and Conditions + Privacy Policy
*/

function CustomPage() {
  return (
    <Layout>
      <PageTitle>Settings and Configurations</PageTitle>
      <Masonary>
        <InstitutionDetials />
        <Integrations />

        <FormCard
          title="Backup and Restore"
          description="Backup your database or restore them"
        >
          <Button tag="button" className="mt-4 mr-2" type="primary">
            Backup database
          </Button>
          <Button tag="button" className="mt-4" type="primary">
            Restore database
          </Button>
        </FormCard>

        <FrontPage />
    
      </Masonary>
    </Layout>
  );
}

export default CustomPage;

function InstitutionDetials() {
  const { data: settings } = useFetchPermenantData<InstitutionSettings>(
    `/api/admin/settings/get?type=${SettingsType.Institution}`
  );
  const { mutate } = useSWRConfig();

  return (
    <FormikCard
      title="Institution Details"
      description="Institution details entered here will be displayed on the frontpage and in the footer."
      initialValues={settings}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: values,
            type: SettingsType.Institution,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Institution}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <TextInput
        label="Institution Name"
        name="name"
        placeholder="Govt Polytechnic College, Perinthalmanna"
      />
      <TextAreaInput
        label="About Institution"
        name="about"
        rows={2}
        placeholder="About institution in few words"
      />
      <TextInput
        label="Institution Address"
        name="address"
        placeholder="Near railway gate, Angadipuram, Perinthalmanna"
      />
      <TextInput
        label="Institution email"
        name="email"
        type="email"
        placeholder="poly@gptcperinthalmanna.in"
      />
      <TextInput
        label="Institution Phone"
        name="phone"
        type="number"
        placeholder="+91 98906532659"
      />
      <TextInput
        label="Institution Website"
        name="website"
        type="text"
        placeholder="https://example.com"
      />
      <TextInput
        label="Logo Url"
        name="logoUrl"
        type="text"
        placeholder="https://example.com/logo.png"
      />
    </FormikCard>
  );
}

function FrontPage() {
  const { data: settings } = useFetchPermenantData<FrontPageSettings>(
    `/api/admin/settings/get?type=${SettingsType.FrontPage}`
  );
  const { mutate } = useSWRConfig();

  return (
    <FormikCard
      title="Front Page settings"
      description="GRS Front page settings and configs"
      initialValues={settings}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: values,
            type: SettingsType.FrontPage,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.FrontPage}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="showInstitutionLogo">
        Show Institution logo on front page
      </CheckBox>
      <CheckBox name="showStudentGrievanceCellMembers">
        Show Student Grievance cell members on front page
      </CheckBox>
      <CheckBox name="showStaffGrievanceCellMembers">
        Show Staff Grievance cell members on front page
      </CheckBox>
      <CheckBox name="showInternalComplaintsCellMembers">
        Show Internal Complaints cell members on front page
      </CheckBox>
      <CheckBox name="showContactDetails">
        Show Contact details on front page
      </CheckBox>
      <TextInput
        label="SEO: Website description"
        name="description"
        placeholder="Lorem ipsum dolor sit amet consectetur adipisicing elit. Nam commodi cumque velit mollitia nihil, ratione aspernatur rem dolorem fugit consectetur ea ipsa vero accusamus delectus. Ullam esse sunt sit dolore."
      />
      <TextInput
        label="SEO: Custom OG Image"
        name="image"
        placeholder="https://seo-og-image.in/sample.png"
      />
      <TextInput
        label="Front page custom background"
        name="backgroundImage"
        placeholder="https://pexels.com/asdlfkja.jpg"
      />
      <TextInput
        label="Google Analytics Code"
        name="analyticsTrackingId"
        placeholder="YOUR_TRACKING_ID"
      />
    </FormikCard>
  );
}

function Integrations() {
  const { data: settings } = useFetchPermenantData<IntegrationSettings>(
    `/api/admin/settings/get?type=${SettingsType.Integration}`
  );
  const { mutate } = useSWRConfig();

  return (
    <FormikCard
      title="Integrations"
      description="Integrations with other services"
      initialValues={{
        whatsappEnabled: settings?.whatsapp.enabled,
        whatsappGroupId: settings?.whatsapp.groupId,
        telegramEnabled: settings?.telegram.enabled,
        telegramChatId: settings?.telegram.groupId,
      }}
      onSubmit={async (values) => {
        try {
          await httpClient.post(`/api/admin/settings/update`, {
            settings: {
              whatsapp: {
                enabled: values.whatsappEnabled,
                groupId: values.whatsappGroupId,
              },
              telegram: {
                enabled: values.telegramEnabled,
                groupId: values.telegramChatId,
              },
            },
            type: SettingsType.Integration,
          });
          toast.success("Updated successfully");
          mutate(`/api/admin/settings/get?type=${SettingsType.Integration}`);
        } catch (error) {
          toastError(error);
        }
      }}
    >
      <CheckBox name="whatsappEnabled">Whatsapp Integration</CheckBox>
      <TextInput
        label="Whatsapp Group ID"
        name="whatsappGroupId"
        placeholder="S23SD9G8S6S87SD887S"
      />
      <CheckBox name="telegramEnabled">Telegram Integration</CheckBox>
      <TextInput
        label="Telegram Group ID"
        name="telegramChatId"
        placeholder="-546554886165135"
      />
    </FormikCard>
  );
}
