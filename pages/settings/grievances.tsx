/* 
    This is the reports page.
    1. Page settings
    2. Category settings
    3. Allowed Priority Levels
    4. Allowed Status
    5. Response Settings -  Self Add
    6. TODO: Approval Settings
    7. TODO: Allow ReOpen
    8. File Upload Settings
    9. TODO: Allow File Upload
*/

import { Select } from "@windmill/react-ui";
import { useState } from "react";
import toast from "react-hot-toast";
import { useSWRConfig } from "swr";
import CheckBox from "../../components/Forms/CheckBox";
import { FormikCard, Masonary } from "../../components/Forms/FormCard";
import TextAreaInput from "../../components/Forms/TextAreaInput";
import TextInput from "../../components/Forms/TextInput";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import { useFetchPermenantData } from "../../lib/adapters/fetcher";
import { SettingsType,  } from "../../lib/constants/settings";
import httpClient from "../../lib/helpers/httpClient";
import { toastError } from "../../lib/helpers/toastError";

function CustomPage() {
  return (
    <Layout>
      <PageTitle>Dashboard</PageTitle>
      <Masonary>
      <GrsPageSettings />
      <GrsTypeSettings type={SettingsType.StaffGrievance}   title={"Staff Grievance Type Settings"}
    description="Here you can configure staff grievances." />
      <GrsTypeSettings type={SettingsType.StudentGrievance}  title={"Student Grievance Type Settings"}
    description="Here you can configure student grievances." />
      <GrsTypeSettings type={SettingsType.InternalGrievance}   title={"Internal Complaints Type Settings"}
    description="Here you can configure internal complaints."/>
      
          {/* Allowed Statuses */}
          {/* Allowed Categories */}
          {/* Allowed Priority Levels */}
      
      </Masonary>
    </Layout>
  );
}

export default CustomPage;


function GrsPageSettings(){
  const [selectedGrs, setSelectedGrs] = useState<SettingsType>(SettingsType.StaffGrievance);
  return(
   <StaffGrs setSelectedGrs={setSelectedGrs} type={selectedGrs} />
  )
}


function StaffGrs({setSelectedGrs, type}: {setSelectedGrs: (grs: SettingsType) => void, type: SettingsType}){

  const { data: settings } = useFetchPermenantData<any>(
    `/api/admin/settings/get?type=${type}`
  );

  const { mutate } = useSWRConfig();
  return (
    <FormikCard
    title="Grievance Page Settings"
    description="Here you can set the page for grievances."
    initialValues={settings}
    onSubmit={async (values) => {
      try {
        await httpClient.post(`/api/admin/settings/update`, {
          settings: values,
          type: type,
        });
        toast.success("Updated successfully");
        mutate(`/api/admin/settings/get?type=${type}`)
      } catch (error) {
        toastError(error);
      }
    }}
    
  >
    <p className="mt-4 mb-1 text-sm">Select Grievance Type</p>
    <Select css="" onChange={(e)=>setSelectedGrs(e.target.value as SettingsType)} >
      <option value={SettingsType.StudentGrievance}>Student Grievances</option>
      <option value={SettingsType.StaffGrievance}>Staff Grievances</option>
      <option value={SettingsType.InternalGrievance}>Internal Complaints</option>
    </Select>
    <CheckBox name="showDate">Show Date</CheckBox>
    <CheckBox name="showTime">Show Time</CheckBox>
    <CheckBox name="showPriority">Show Priority</CheckBox>
    <CheckBox name="showStatus">Show Status</CheckBox>
    <CheckBox name="showCategory">Show Category</CheckBox>
    <CheckBox name="showIpAddress">Show IP</CheckBox>
    <TextInput
      label="Info Card Title"
      name="infoCardTitle"
      placeholder="Greivance Info"
    />
    <TextInput
      label="Content Title"
      name="contentTitle"
      placeholder="Greivance Content"
    />
    <TextInput
      label="Responses Title"
      name="responsesTitle"
      placeholder="Responses"
    />
  </FormikCard>
  )
}

function GrsTypeSettings({ type, title, description}: {type: SettingsType; title: string; description:string}){
  const { data: settings } = useFetchPermenantData<any>(
    `/api/admin/settings/get?type=${type}`
  );

  const { mutate } = useSWRConfig();


  return(
    <FormikCard
    title={title}
    description={description}
    initialValues={{...settings,
      allowedCategories: settings?.allowedCategories.map((cat:any)=>cat.name).join(";\n"),
    }}
    onSubmit={async (values) => {
      try {
        await httpClient.post(`/api/admin/settings/update`, {
          settings: {...values, allowedCategories: values.allowedCategories.split(";").map((cat:string)=>{return({name:cat.replace(/\n/g, ''),value: cat.toLowerCase().replace(/\s/g, '_').replace(/\n/g, '')})})},
          type,
        });
        toast.success("Updated successfully");
        mutate(`/api/admin/settings/get?type=${type}`)
      } catch (error) {
        toastError(error);
      }
    }}
  >
    <TextInput
      label="Maximum content length"
      name="contentLengthMax"
      type="number"
      placeholder="1000"
    />
    <TextInput
      label="Minimum content length"
      name="contentLengthMin"
      placeholder="15"
    />
    <CheckBox name="allowImageUpload">
      Allow Image upload for grievances
    </CheckBox>
    <CheckBox name="allowAuthorResponse">
      Allow author to create response
    </CheckBox>
    <TextAreaInput label="Allowed categories" rows={12} name={"allowedCategories"} placeholder="Academic Related complaints; Admission Related complaints" />
    {/* Allowed Statuses */}
    {/* Allowed Categories */}
    {/* Allowed Priority Levels */}
</FormikCard> 
  )
}