import { useRouter } from "next/router";
import React from "react";
import { IconType } from "react-icons";
import { CgBoy, CgUser } from "react-icons/cg";
import { HiOutlineUserCircle } from "react-icons/hi";
import { useSWRConfig } from "swr";

import ParentProfileEditForm from "../components/Custom/ParentProfileEditForm";
import PasswordForm from "../components/Custom/PasswordForm";
import StaffProfileEditForm from "../components/Custom/StaffProfileEditForm";
import StudentProfileEditForm from "../components/Custom/StudentProfileEditForm";
import { toastError } from "../lib/helpers/toastError";
import { useMyProfile } from "../lib/hooks/useMyProfile";
import { UserProfileFullType } from "../lib/types/db";

type section = "profile" | "password";
type userType = "student" | "parent" | "staff";

function CustomPage() {
  const [type, setType] = React.useState<userType>(() => "student");
  const [section, setSection] = React.useState<section>("profile");
  const router = useRouter();
  const profile = useMyProfile();
  const { mutate } = useSWRConfig();

  const handleProfileUpdate = async () => {
    try {
      const _profile = await mutate<UserProfileFullType>(
        "/api/users/me"
      );
      if (_profile && !_profile?.hasPassword) {
        setSection("password");
      } else {
        router.push("/dashboard");
      }
    } catch (error) {
      toastError(error)
    }
  };

  const handlePasswordUpdate = () => {
    router.push("/dashboard");
  };

  let Form: any;
  switch (type) {
    case "student":
      Form = StudentProfileEditForm;
      break;
    case "staff":
      Form = StaffProfileEditForm;
      break;
    case "parent":
      Form = ParentProfileEditForm;
      break;
    default:
      Form = StudentProfileEditForm;
      break;
  }

  const ProfileSection = () => (
    <>
      <h2 className="mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200">
        Select account type - <span className="capitalize">{type}</span>
      </h2>
      <div className="flex">
        <UserCard
          icon={CgBoy}
          title="I am a student"
          onClick={() => setType("student")}
          active={type === "student"}
        />
        <UserCard
          icon={HiOutlineUserCircle}
          title="I am a staff"
          onClick={() => setType("staff")}
          active={type === "staff"}
        />
        <UserCard
          icon={CgUser}
          title="I am a parent"
          onClick={() => setType("parent")}
          active={type === "parent"}
        />
      </div>

      <div className="mt-10">
        <Form onSuccess={handleProfileUpdate} />
      </div>
    </>
  );

  const SetPasswordSection = () => (
    <>
      <h2 className="mb-4 text-xl font-semibold text-gray-700 dark:text-gray-200">
        Create new password - <span className="capitalize"> {type} </span>
      </h2>
      {profile && <PasswordForm isNew onSuccess={handlePasswordUpdate} />}
    </>
  );

  return (
    <div className="flex items-center min-h-screen p-6 bg-gray-50 dark:bg-gray-900">
      <div className="flex-1 h-full max-w-4xl mx-auto overflow-hidden bg-white rounded-lg shadow-xl dark:bg-gray-800">
        <div className="flex flex-col overflow-y-auto md:flex-row">
          <main className="flex items-center justify-center w-full p-6 sm:p-12">
            <div className="w-full">
              <h1 className="block mb-4 text-2xl font-semibold text-gray-700 dark:text-gray-200">
                Complete your Registration
              </h1>
              {section === "profile" && <ProfileSection />}
              {section === "password" && <SetPasswordSection />}
            </div>
            <div></div>
          </main>
        </div>
      </div>
    </div>
  );
}

export default CustomPage;

function UserCard({
  icon,
  title,
  onClick,
  active = false,
}: {
  icon: IconType;
  title: string;
  onClick?: () => void;
  active?: boolean;
}) {
  const Icon = icon;
  return (
    <div className="w-1/3 px-3">
      <div
        className={
          "text-gray-700 cursor-pointer dark:text-gray-200 hover:bg-gray-500 hover:bg-opacity-25" +
          (active ? " bg-gray-500 bg-opacity-30" : "")
        }
        onClick={onClick}
      >
        <div className="flex flex-col items-center justify-center p-2 border-2 lg:p-4 rounded-xl">
          <Icon
            className="hidden text-gray-700 dark:text-gray-200 lg:block"
            color="white"
            size="120"
          />
          <p className="text-xs font-bold sm:text-base lg:text-xl">{title}</p>
        </div>
      </div>
    </div>
  );
}
