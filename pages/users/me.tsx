import React from "react";
import Image from "next/image";
import { Badge, Button } from "@windmill/react-ui";
import { useRouter } from "next/router";

import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import FormWrapper from "../../components/Forms/FormWrapper";
import { useMyProfile } from "../../lib/hooks/useMyProfile";

function CustomPage() {
  const profile = useMyProfile();
  const router = useRouter();
  if (!profile) {
    return (
      <Layout>
        <PageTitle>Loading..</PageTitle>
      </Layout>
    );
  }

  const _profile = { ...profile } as { [key: string]: any };

  return (
    <Layout>
      <PageTitle>My Profile</PageTitle>
      <div className="flex flex-col md:flex-row-reverse">
        <div className="flex justify-center flex-grow my-5">
          <div>
            <div className="relative w-40 h-40 mx-auto overflow-hidden rounded-full lg:w-60 lg:h-60">
              <Image
                src={profile.image}
                alt="profile"
                layout="fill"
                objectFit="cover"
              />
            </div>
            <div className="font-bold text-center ">
              <div className="mt-4 text-3xl font-bold text-center">
                {profile.name && profile.name}
              </div>
              <p>
                {_profile.type && _profile.type === "student"
                  ? _profile.department
                  : _profile.type === "parent"
                  ? "Parent"
                  : _profile.type === "staff"
                  ? `${_profile.designation}, ${_profile.department}`
                  : "Unknown"}
              </p>
              <Badge>Staff</Badge>
            </div>
          </div>
        </div>
        <div className="w-full mx-auto md:mx-0 md:w-2/3 lg:w-2/4 xl:w-2/5 2xl:w-2/6">
          <FormWrapper>
            <div className="flex justify-center">
              <div className="w-full">
                {profile &&
                  Object.keys(profile).map((key) => {
                    if (typeof _profile[key] !== "string") return null;
                    return (
                      <Field label={key} key={key} value={_profile[key]} />
                    );
                  })}
              </div>
            </div>
          </FormWrapper>
        </div>
      </div>
      <div className="flex ml-1">
        <Button
          block={false}
          type="button"
          className="mr-2"
          onClick={() => router.push("/users/edit/")}
        >
          Edit Profile
        </Button>
        <Button
          block={false}
          type="button"
          className="mr-2"
          onClick={() => router.push("/users/edit/")}
        >
          Change Password
        </Button>
      </div>
    </Layout>
  );
}

export default CustomPage;

function Field({ label, value }: { label: string; value: string }) {
  return (
    <div className="flex justify-between w-full max-w-md py-2 border-t-2">
      <div className="capitalize">{label}</div>
      <div>
        {value.slice(0, 30)}
        {value.length > 30 ? "..." : null}
      </div>
    </div>
  );
}
