import { Form, Formik } from "formik";
import React from "react";
import StudentProfileEditForm from "../../components/Custom/StudentProfileEditForm";

import TextInput from "../../components/Forms/TextInput";
import PageTitle from "../../components/Typography/PageTitle";
import Title from "../../components/Ui/Title";
import Layout from "../../containers/Layout";
import FormWrapper from "../../components/Forms/FormWrapper";
import StaffProfileEditForm from "../../components/Custom/StaffProfileEditForm";
import ParentProfileEditForm from "../../components/Custom/ParentProfileEditForm";
import PasswordForm from "../../components/Custom/PasswordForm";
import { useMyProfile } from "../../lib/hooks/useMyProfile";

function CustomPage() {
  const profile = useMyProfile();

  return (
    <Layout>
      <PageTitle>Edit User</PageTitle>

      <FormWrapper>
        <Title>Basic Details</Title>
        <Formik
          initialValues={{
            ...profile,
          }}
          onSubmit={() => {
            return;
          }}
          enableReinitialize
        >
          <Form>
            <div className="flex flex-wrap w-full mb-8">
              <MediumBlock>
                <TextInput name="name" label="Your Name" disabled />
              </MediumBlock>
              <MediumBlock>
                <TextInput name="email" label="Email" disabled />
              </MediumBlock>
            </div>
          </Form>
        </Formik>
      </FormWrapper>
      <FormWrapper>
        {profile && profile.type === "student" && (
          <StudentProfileEditForm initialValues={{ ...profile }} />
        )}
        {profile && profile.type === "staff" && (
          <StaffProfileEditForm initialValues={{ ...profile }} />
        )}
        {profile && profile.type === "parent" && <ParentProfileEditForm initialValues={{...profile}} />}
      </FormWrapper>
      <FormWrapper>
        <PasswordForm />
      </FormWrapper>
    </Layout>
  );
}

export default CustomPage;

function MediumBlock({
  children,
  block = false,
}: {
  children: React.ReactNode;
  block?: boolean;
}) {
  const comp = (
    <div className="w-full px-2 my-2 lg:w-1/2 2xl:w-1/3">{children}</div>
  );

  return block ? <div className="w-full">{comp}</div> : comp;
}
