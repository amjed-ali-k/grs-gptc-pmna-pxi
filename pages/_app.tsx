import React from "react";
import { Windmill } from "@windmill/react-ui";
import { SessionProvider } from "next-auth/react";
import "../styles/globals.css";
import NProgress from "nprogress";
import Router from "next/router";
import "nprogress/nprogress.css";

NProgress.configure({
  minimum: 0.3,
  easing: "ease",
  speed: 800,
  showSpinner: false,
});

Router.events.on("routeChangeStart", () => NProgress.start());
Router.events.on("routeChangeComplete", () => NProgress.done());
Router.events.on("routeChangeError", () => NProgress.done());

import type { AppProps } from "next/app";

function MyApp({ Component, pageProps: { session, ...pageProps } }: AppProps) {
  // suppress useLayoutEffect warnings when running outside a browser
  if (!process.browser) React.useLayoutEffect = React.useEffect;

  return (
    <SessionProvider session={session}>
      <Windmill usePreferences>
        <Component {...pageProps} />
      </Windmill>
    </SessionProvider>
  );
}

export default MyApp;
