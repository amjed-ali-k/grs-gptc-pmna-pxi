import React from "react";
import { useSWRConfig } from "swr";
import { Button } from "@windmill/react-ui";
import {
  AiOutlineFieldTime,
  AiOutlineUser,
  AiFillPlusCircle,
} from "react-icons/ai";
import { useRouter } from "next/router";
import { GrStatusWarningSmall } from "react-icons/gr";
import {
  MdOutlineCategory,
  MdLowPriority,
  MdEditLocation,
} from "react-icons/md";
import { GiEarthAsiaOceania } from "react-icons/gi";
import { BsCalendarDate } from "react-icons/bs";
import _d from "dayjs";
import * as Yup from "yup";
import toast from "react-hot-toast";
import { Form, Formik } from "formik";

import PageTitle from "../../../components/Typography/PageTitle";
import Title from "../../../components/Ui/Title";
import Layout from "../../../containers/Layout";
import { GrievanceType } from "../../../lib/types/db";
import httpClient from "../../../lib/helpers/httpClient";
import { GrTypeType, useStatusField } from "../../../lib/constants/grievance";
import TextAreaInput from "../../../components/Forms/TextAreaInput";
import SelectInput from "../../../components/Forms/SelectInput";
import FormButton from "../../../components/Forms/FormButton";
import {
  getResponseSchema,
  responseSchema,
} from "../../../lib/schemas/grievances";
import ResponsesList from "../../../components/Custom/ResponsesList";
import UserName from "../../../components/Ui/UserName";
import { toastError } from "../../../lib/helpers/toastError";
import { useFetchPermenantData } from "../../../lib/adapters/fetcher";
import { GrievanceSettings } from "../../../lib/constants/settings";
import { useMyProfile } from "../../../lib/hooks/useMyProfile";
import { Permissions } from "../../../lib/constants/users";

function CustomPage() {
  // const [grs, setGrs] = React.useState<GrievanceType | null>(() => null);
  const [refresh, setRefresh] = React.useState<boolean>(() => false);
  const router = useRouter();
  const { mutate } = useSWRConfig();
  const { id } = router.query;

  const { data: grs } = useFetchPermenantData<GrievanceType>(
    `/api/grievances/${id}`
  );

  const { data: settings } = useFetchPermenantData<GrievanceSettings>(
    "/api/settings/grievances/student"
  );

  function reload() {
    setRefresh(!refresh);
    mutate(`api/grievances/${id}`, grs, false);
  }

  return (
    <Layout>
      <PageTitle>{grs ? grs.title : null}</PageTitle>
      <div className="flex flex-wrap w-full lg:flex-row-reverse">
        <div className="w-full px-2 lg:w-1/2 2xl:w-1/3">
          <Title>{settings ? settings.infoCardTitle : "Grievance Info"}</Title>
          <div className="relative p-3 my-3 overflow-hidden bg-gray-100 border-2 border-l-8 rounded-lg dark:border-gray-500 dark:bg-gray-800 dark:text-white">
            <GiEarthAsiaOceania
              className="absolute text-black -right-20 -bottom-25 dark:text-white opacity-20"
              size={250}
            />
            {settings?.showDate && (
              <div className="font-base">
                <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                  <BsCalendarDate className="mr-1" /> Date:
                </span>
                {grs ? _d(new Date(grs.createdAt)).format("D MMMM YYYY") : null}
              </div>
            )}
            {settings?.showTime && (
              <div className="font-base">
                <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                  <AiOutlineFieldTime className="mr-1" /> Time:
                </span>
                {grs ? _d(new Date(grs.createdAt)).format("h:mm A") : null}
              </div>
            )}

            <div className="font-base">
              <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                <AiOutlineUser className="mr-1" />
                Author:
              </span>
              <UserName id={grs?.userId} />
            </div>
            {settings?.showStatus && (
              <div className="capitalize font-base">
                <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                  <GrStatusWarningSmall className="mr-1" />
                  Status:
                </span>
                {grs?.status}
              </div>
            )}
            {settings?.showCategory && (
              <div className="font-base">
                <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                  <MdOutlineCategory className="mr-1" />
                  Category:
                </span>
                {grs?.category.replace(/\b\w/g, (l) => l.toUpperCase().replace("_", " "))}
              </div>
            )}
            {settings?.showPriority && (
              <div className="font-base">
                <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                  <MdLowPriority className="mr-1" />
                  Priority:
                </span>
                {grs?.priority}
              </div>
            )}
            {settings?.showIpAddress && (
              <div className="font-base">
                <span className="inline-flex items-center pr-4 font-bold dark:text-slate-300 text-slate-700">
                  <MdEditLocation className="mr-1" />
                  IP:
                </span>
                {grs?.ip}
              </div>
            )}
          </div>
        </div>
        <div className="w-full px-2 lg:w-1/2 2xl:w-2/3">
          <div className="w-full ">
            {/* Grievance Content  */}
            <div className="mb-12 whitespace-pre-line">
              <Title>{settings?.contentTitle}</Title>
              <p>{grs?.content}</p>
            </div>

            <div>
              <Title>{settings?.responsesTitle}</Title>

              <ResponsesList
                grievanceId={grs?.key}
                grievanceType={grs?.type}
                reload={refresh}
              />
            </div>

            {/* Response Form  */}
            <div className="">
              {grs && (
                <NewResponseWrapper
                  grievance={grs}
                  status={grs?.status}
                  onSuccess={reload}
                  settings={settings}
                />
              )}
            </div>
          </div>
        </div>
      </div>
    </Layout>
  );
}

export default CustomPage;

function NewResponseWrapper(props: {
  grievance?: GrievanceType;
  status?: string;
  onSuccess?: () => void;
  settings?: GrievanceSettings;
}) {
  const _onSuccess = () => {
    setShowForm(false);
    props.onSuccess && props.onSuccess();
  };
  const [showForm, setShowForm] = React.useState(false);

  // Permissions
  const profile = useMyProfile();
  const canAddResponse = () => {
    if (
      (props.grievance?.type === GrTypeType.STAFF &&
        profile?.permissions.includes(
          Permissions.CREATE_STAFF_GRIEVANCE_RESPONSE
        )) ||
      (props.grievance?.type === GrTypeType.STUDENT &&
        profile?.permissions.includes(
          Permissions.CREATE_STUDENT_GRIEVANCE_RESPONSE
        )) ||
      (props.grievance?.type === GrTypeType.INTERNAL &&
        profile?.permissions.includes(
          Permissions.CREATE_INTERNAL_COMPLAINTS_RESPONSE
        ))
    ) {
      return true;
    }

    if (
      props.grievance?.userId === profile?.id &&
      props.settings?.allowAuthorResponse
    )
      return true;
    return false;
  };

  if (!canAddResponse()) return null;

  return (
    <>
      {!showForm && (
        <div className="block w-full pt-3 border-t-2 dark:border-gray-700">
          <div
            onClick={() => {
              setShowForm(!showForm);
              window.scroll({
                top: document.body.offsetHeight,
                left: 0,
                behavior: "smooth",
              });
            }}
            className="flex items-center max-w-xs p-2 bg-teal-400 border-2 rounded-lg cursor-pointer dark:bg-teal-700 mb-7 hover:bg-teal-500 hover:bg-opacity-30"
          >
            <AiFillPlusCircle size={30} className="mr-3" /> Add new response
          </div>
        </div>
      )}
      {showForm && (
        <NewResponse
          {...props}
          grievanceId={props.grievance?.key}
          type={props.grievance?.type}
          onSuccess={_onSuccess}
          onCancel={() => setShowForm(false)}
        />
      )}
    </>
  );
}

function NewResponse({
  grievanceId,
  status,
  onSuccess,
  onCancel,
  type
}: {
  grievanceId?: string;
  type?: string;
  status?: string;
  onSuccess?: () => void;
  onCancel: () => void;
}) {
  const { data: settings } = useFetchPermenantData<GrievanceSettings>(
    `/api/settings/grievances/${type}`
  );

  return (
    <div className="p-4 my-3 border-2 rounded-md mb-7 bg-gray-50 dark:bg-gray-800">
      <Title>Add Response</Title>
      <Formik
        initialValues={{
          content: "",
          status: "",
          grievanceId: grievanceId,
        }}
        enableReinitialize
        validationSchema={Yup.object(
          settings ? getResponseSchema(settings) : responseSchema
        )}
        onSubmit={async (values, { setSubmitting }) => {
          try {
            await httpClient.post("/api/responses/new/", values);
            toast.success("Successfully added new response!");
            onSuccess && onSuccess();
          } catch (error) {
            toastError(error);
          }
          setSubmitting(false);
        }}
      >
        <Form>
          <div className="lg:flex lg:space-x-4">
            <div className="w-full lg:w-1/2">
              <SelectInput label="Change status to" name="status">
                <option>Select</option>
                <option value={status}>No change</option>
                {useStatusField().map((st) => (
                  <option key={st} value={st} className="capitalize">
                    {st}{" "}
                  </option>
                ))}
              </SelectInput>
            </div>
          </div>

          <TextAreaInput
            label="Enter your response in detail"
            placeholder="Your grievance is addressed and ..."
            name="content"
            rows={6}
          />
          <div className="flex items-center space-x-4">
            <div className="max-w-sm">
              <FormButton>Submit response</FormButton>
            </div>
            <Button className="mt-4" type="reset" onClick={onCancel}>
              {" "}
              Cancel{" "}
            </Button>
          </div>
        </Form>
      </Formik>
    </div>
  );
}
