import React from "react";
import AlertBanner from "../../../components/Cards/AlertBanner";
import AllGrievances from "../../../components/Tables/AllGrievances";
import PageTitle from "../../../components/Typography/PageTitle";
import Layout from "../../../containers/Layout";
import { GrTypeType } from "../../../lib/constants/grievance";


function CustomPage() {
  return (
    <Layout>
      <PageTitle>All Internal Complaints</PageTitle>
      <AlertBanner>
        <p>
          <span className="pr-2 font-bold"> All Internal Complaints! </span>
          <span>In this page you can view all Internal Complaints posted.</span>
        </p>
      </AlertBanner>
      <div className="flex flex-wrap w-full mb-4 lg:flex-row-reverse">
        <div className="w-full ">
          <AllGrievances type={GrTypeType.INTERNAL} />
        </div>
      </div>
    </Layout>
  );
}

export default CustomPage;
