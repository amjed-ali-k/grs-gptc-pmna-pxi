import React from "react";
import AlertBanner from "../../components/Cards/AlertBanner";
import AllGrievances from "../../components/Tables/AllGrievances";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";

function CustomPage() {
  return (
    <Layout>
      <PageTitle>All Grievances</PageTitle>
      <AlertBanner>
        <p>
          <span className="pr-2 font-bold"> All Grievances! </span>
          <span>In this page you can view all grievances posted by students.</span>
        </p>
      </AlertBanner>
      <div className="flex flex-wrap w-full mb-4 lg:flex-row-reverse">
        <div className="w-full ">
          <AllGrievances />
        </div>
      </div>
    </Layout>
  );
}

export default CustomPage;
