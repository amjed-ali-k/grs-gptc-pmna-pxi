import { Form, Formik } from "formik";
import React from "react";
import Link from "next/link";
import * as Yup from "yup";
import toast from "react-hot-toast";
import { useRouter } from "next/router";

import AlertBanner from "../../../components/Cards/AlertBanner";
import FormButton from "../../../components/Forms/FormButton";
import SelectInput from "../../../components/Forms/SelectInput";
import TextAreaInput from "../../../components/Forms/TextAreaInput";
import TextInput from "../../../components/Forms/TextInput";
import PageTitle from "../../../components/Typography/PageTitle";
import Title from "../../../components/Ui/Title";
import Layout from "../../../containers/Layout";
import {
  getGrievanceSchema,
  grievanceSchema,
} from "../../../lib/schemas/grievances";
import CheckBox from "../../../components/Forms/CheckBox";
import httpClient from "../../../lib/helpers/httpClient";
import { GrTypeType } from "../../../lib/constants/grievance";
import { useFetchPermenantData } from "../../../lib/adapters/fetcher";
import { StudentGrievanceSettings } from "../../../lib/constants/settings";

function CustomPage() {
  const router = useRouter();
  const { data } = useFetchPermenantData<StudentGrievanceSettings>(
    "/api/settings/grievances/student"
  );
  const schema = data ? getGrievanceSchema(data) : grievanceSchema;
  return (
    <Layout>
      <PageTitle>New Greivance</PageTitle>
      <AlertBanner>
        <p>
          <span className="pr-2 font-bold"> Welcome! </span>
          <span>
            You are about to create a new grievance. Please fill out the form.
          </span>
        </p>
      </AlertBanner>
      <Formik
        initialValues={{
          title: "",
          content: "",
          priority: "",
          category: "",
          acceptedTerms: false, // added for our checkbox
          type: GrTypeType.STUDENT,
        }}
        enableReinitialize
        validationSchema={Yup.object(schema)}
        onSubmit={async (values, { setSubmitting }) => {
          try {
            await httpClient.post("/api/grievances/new/", values);
            toast.success("Successfully created a grievance!");
            router.push("/grievances/");
          } catch (error) {
            toast.error("An error occured!");
          }
          setSubmitting(false);
        }}
      >
        <Form>
          <div className="w-full">
            <Title>Fill out the form</Title>
            <TextInput
              label="Subject"
              name="title"
              type="text"
              placeholder="There is a problem with..."
            />
            <div className="lg:flex lg:space-x-4">
              <div className="w-full lg:w-1/2">
                <SelectInput label="Priority" name="priority">
                  <option>Select priority</option>
                  {data?.allowedPriorities.map((pr) => (
                    <option key={pr.value} value={pr.value}>
                      {pr.name}
                    </option>
                  ))}
                </SelectInput>
              </div>
              <div className="w-full lg:w-1/2">
                <SelectInput label="Category" name="category">
                  <option>Select Category</option>
                  {data?.allowedCategories.map((e) => {
                    return (
                      <option key={e.value} value={e.value}>
                        {e.name}
                      </option>
                    );
                  })}
                </SelectInput>
              </div>
            </div>
            <TextAreaInput
              className="mt-1"
              placeholder="The problem is ..."
              rows={6}
              name="content"
              label="Enter your Grievance in detail"
            />
            <CheckBox name="acceptedTerms">
              I agree to the{" "}
              <Link href="/grievances/terms-and-conditions">
                <a className="underline">Terms and conditions</a>
              </Link>{" "}
              for submiting grievance.
            </CheckBox>

            <div className="max-w-sm">
              <FormButton>Submit New Greivance</FormButton>
            </div>
          </div>
        </Form>
      </Formik>
    </Layout>
  );
}

export default CustomPage;
