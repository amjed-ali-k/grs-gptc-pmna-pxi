import React from "react";
import Link from "next/link";

import AlertBanner from "../../components/Cards/AlertBanner";
import PageTitle from "../../components/Typography/PageTitle";
import Layout from "../../containers/Layout";
import FormWrapper from "../../components/Forms/FormWrapper";
import { CgBoy } from "react-icons/cg";
import { FaUserTie } from "react-icons/fa";
import { IoIosPaper } from "react-icons/io";
import { useMyProfile } from "../../lib/hooks/useMyProfile";
import { Permissions } from "../../lib/constants/users";

function CustomPage() {
  const profile = useMyProfile();
  return (
    <Layout>
      <PageTitle>Select Greivance Type</PageTitle>
      <AlertBanner>
        <p>
          <span className="pr-2 font-bold"> </span>
          <span>Select grievance type which you are about to create</span>
        </p>
      </AlertBanner>
      <div className="flex flex-col justify-center lg:flex-row">
        {profile?.permissions.includes(
          Permissions.CREATE_STUDENT_GRIEVANCE
        ) && (
          <Link href="/grievances/student-grievance/new">
            <a className="w-full px-3 cursor-pointer lg:w-1/3 hover:opacity-100 opacity-80">
              <FormWrapper>
                <div className="py-4 text-center">
                  <div className="flex justify-center">
                    <CgBoy size={140} />
                  </div>
                  <div className="text-xl font-bold">Student Grievance</div>
                </div>
              </FormWrapper>
            </a>
          </Link>
        )}
        {profile?.permissions.includes(Permissions.CREATE_STAFF_GRIEVANCE) && (
          <Link href="/grievances/staff-grievance/new">
            <a className="w-full px-3 cursor-pointer lg:w-1/3 hover:opacity-100 opacity-80">
              <FormWrapper>
                <div className="py-4 text-center">
                  <div className="flex justify-center">
                    <FaUserTie size={140} />
                  </div>
                  <div className="text-xl font-bold">Staff Grievance</div>
                </div>
              </FormWrapper>
            </a>
          </Link>
        )}
        {profile?.permissions.includes(
          Permissions.CREATE_INTERNAL_COMPLAINTS
        ) && (
          <Link href="/grievances/internal-complaints/new">
            <a className="w-full px-3 cursor-pointer lg:w-1/3 hover:opacity-100 opacity-80">
              <FormWrapper>
                <div className="py-4 text-center">
                  <div className="flex justify-center">
                    <IoIosPaper size={140} />
                  </div>
                  <div className="text-xl font-bold">Internal Complaint</div>
                </div>
              </FormWrapper>
            </a>
          </Link>
        )}
      </div>
    </Layout>
  );
}

export default CustomPage;
